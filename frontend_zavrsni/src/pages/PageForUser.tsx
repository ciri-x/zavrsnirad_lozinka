import React, { Component } from "react";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { Switch, Route, Redirect } from "react-router-dom";
import jwt from "jwt-decode";
import Info from "../components/info/AboutPage";
import Tester from "../components/forUser/PasswordTester"
import Generator from "../components/forUser/PasswordGenerator"
import Quiz from "../components/forUser/PasswordQuiz"
import QuizGen from "../components/forUser/PasswordQuizGen"
import Profile from "../components/forUser/UserProfile"
import logo from "../components/info/styles/logoSmall.png";
import {getUsersById} from "../store/UserProfileService";

export class PageForUser extends Component {
    state = {
        username: "",
        odjava: false
    };

    private userID = 0;
    private userInit = false;

    private getUserByID = (
        userID: any,
    ) => {
        getUsersById(userID)
            .then(res => {
                if (res.status === 200) {
                    this.setState({username: res.data.username});
                    this.userInit = true;
                    console.log(this.state.username)
                }
            })
            .catch(error => {
                console.log(error);
            });
        }
    ;

    render() {

        if (this.state.odjava) {
            return <Redirect to={"/"} />;
        }
        const item = sessionStorage.getItem("userInfo");
        if (item) {
            const token = jwt(item!);
            if (token) {
                // @ts-ignore
                const role = token.role;
                const currentID = token.id;
                this.userID = currentID;
                if(!this.userInit){
                    this.getUserByID(this.userID);
                }
                if (role !== "CITIZEN") {
                    return <Redirect to="/" />;
                }
            }
        } else {
            return <Redirect to="/" />;
        }
        return (
            <div>
                <Navbar className={"NavLozinka"} sticky={"top"} collapseOnSelect expand="lg">
                    <Navbar.Brand href="/pocetnaKorisnik/">
                        <img
                            src={logo}
                            width="120"
                            height="40"
                            alt="logo"
                            className="d-inline-block align-top"
                        />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">

                        <Nav className="NavLozinka">
                            <Nav.Link href="/pocetnaKorisnik/kviz">Kviz</Nav.Link>
                            <Nav.Link href="/pocetnaKorisnik/tester">Tester</Nav.Link>
                            <Nav.Link href="/pocetnaKorisnik/generator">Generator</Nav.Link>
                            <Nav.Link href="/pocetnaKorisnik/profil">{this.state.username}</Nav.Link>
                            <Nav.Link eventKey={2} onClick={this.odjava}>
                                <button>Odjava</button>
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                <Switch>
                    <Route path="/pocetnaKorisnik/tester" component={Tester} />
                    <Route path="/pocetnaKorisnik/generator" component={Generator} />
                    <Route path="/pocetnaKorisnik/kviz" component={Quiz} />
                    <Route path="/pocetnaKorisnik/kvizGen" component={QuizGen} />
                    <Route path="/pocetnaKorisnik/profil" component={Profile} />
                    <Route path="/pocetnaKorisnik" component={Info} />
                </Switch>
            </div>
        );
    }

    odjava = () => {
        sessionStorage.clear();
        this.setState({ odjava: true });
    };
}
