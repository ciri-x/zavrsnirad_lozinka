import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import "./design/NewPassword.css";
import {Redirect } from "react-router-dom";
// @ts-ignore
import logo_png from "../components/info/styles/background.png";
import {Alert, Button, Card, ListGroup, OverlayTrigger, ProgressBar, Tooltip} from "react-bootstrap";
import InputGroup from "react-bootstrap/InputGroup";
import {resetUsername, confirmChange} from "../store/LoginService";
import ReactHtmlParser from "react-html-parser";


export class ResetUsername extends Component {
    state = {
        citizen: true,
        email: ""
    };

    private mailIsGiven = true;
    private goToChangeUsername = false;

    private callAction = (dispatch: any) => {
        if(this.state.email.length===0){
            this.mailIsGiven = false;
            setTimeout(() => {
                this.mailIsGiven = true; this.setState({render: false})
            }, 3000);
        }
        else{
            this.mailIsGiven = true;
        }

        this.sendMailResetUsername(this.state.email);

    };

    private sendMailResetUsername = (
        email:any
    ) => {
        if(this.mailIsGiven) {
            resetUsername(email)
                .then(res => {
                    if (res === true) {
                        this.goToChangeUsername = true;
                        this.setState({ ...this.state });
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        }
        else{
            this.setState({ ...this.state });
        }
    };


    onChange = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {

        if (this.goToChangeUsername) {
            return <Redirect to="/promjenaNadimka" />;
        }
        const proper = { "overflowY": "hidden" ,backgroundImage: `url(${logo_png})` } as React.CSSProperties;
        const htmlTextNewPass =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:2.5vh;color: white;font-weight: bold">Unesite mail adresu aktivnog korisnika kojem ste zaboravili nadimak! </p>\n' +
            "\n" +
            "        </div>\n" +
            "    </div>";

        return (
            <div className="AppNewPassword" style={ proper}>
                <div className="AppNewPassword-content">
                    <Card  className="CardNewPass" bg="success">
                        <Card.Header className={"CardTitle"}>Novi nadimak</Card.Header>
                        <div className={"TextNewPass"}>
                            {ReactHtmlParser(htmlTextNewPass)}
                        </div>
                        <Card.Body className={"CardBodyNewPass"}>
                            <Form.Group controlId="formTitle" className="mailInput">
                                <InputGroup className="Title">
                                    <InputGroup.Text>E-mail</InputGroup.Text>
                                    <Form.Control
                                        className={"inputField"}
                                        autoComplete="off"
                                        type="text"
                                        placeholder="Upišite vašu mail adresu"
                                        aria-describedby="inputGroupPrepend"
                                        name="email"
                                        value={this.state.email}
                                        onChange={this.onChange}
                                    />
                                </InputGroup>
                            </Form.Group>
                            {!this.mailIsGiven ? (
                                <Alert variant={"warning"}>
                                    Upišite mail!
                                </Alert>
                            ) : null}
                            <InputGroup>
                                <Button
                                    variant="warning"
                                    onClick={this.callAction}
                                    className="btnSend"
                                >
                                    Pošalji
                                </Button>
                                <Button
                                    variant="danger"
                                    href={"prijava"}
                                    className="btnSend"
                                >
                                    Odustani
                                </Button>
                            </InputGroup>
                        </Card.Body>
                    </Card>
                </div>
            </div>

        );
    }

}
