import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import "./design/NewPassword.css";
import {Redirect } from "react-router-dom";
// @ts-ignore
import logo_png from "../components/info/styles/background.png";
import {Alert, Button, Card} from "react-bootstrap";
import InputGroup from "react-bootstrap/InputGroup";
import {confirm} from "../store/LoginService";
import ReactHtmlParser from "react-html-parser";


export class Confirm extends Component {
    state = {
        citizen: true,
        tokenConf: ""
    };

    private tokenIsGiven = true;
    private goToLogin = false;

    private callAction = (dispatch: any) => {
        if(this.state.tokenConf.length===0){
            this.tokenIsGiven = false;
            setTimeout(() => {
                this.tokenIsGiven = true; this.setState({render: false})
            }, 3000);
        }
        else{
            this.tokenIsGiven = true;
        }

        this.confirmWithToken(this.state.tokenConf);

    };

    private confirmWithToken = (
        token:any
    ) => {
        if(this.tokenIsGiven) {
            confirm(token)
                .then(res => {
                    if (res === true) {
                        this.goToLogin = true;
                        this.setState({ ...this.state });
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        }
        else{
            this.setState({ ...this.state });
        }
    };


    onChange = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {

        if (this.goToLogin) {
            return <Redirect to="/prijava" />;
        }
        const proper = { "overflowY": "hidden" ,backgroundImage: `url(${logo_png})` } as React.CSSProperties;
        const htmlTextConfirm =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:2.5vh;color: white;font-weight: bold">Primili ste mail sa aktivacijskim kodom!</p>\n' +
            "\n" +
            "        </div>\n" +
            "    </div>";
        return (
            <div className="AppNewPassword" style={ proper}>
                <div className="AppNewPassword-content">
                    <Card  className="CardNewPass" bg="success">
                        <Card.Header className={"CardTitle"}>Aktivacija</Card.Header>
                        <div className={"TextNewPass"}>
                            {ReactHtmlParser(htmlTextConfirm)}
                        </div>
                        <Card.Body className={"CardBodyNewPass"}>
                            <Form.Group controlId="formTitle" className="mailInput">
                                <InputGroup className="Title">
                                    <InputGroup.Text>Aktivacijski Kod</InputGroup.Text>
                                        <Form.Control
                                            className={"inputField"}
                                            autoComplete="off"
                                            type="text"
                                            placeholder="Upišite aktivacijski kod"
                                            aria-describedby="inputGroupPrepend"
                                            name="tokenConf"
                                            value={this.state.tokenConf}
                                            onChange={this.onChange}
                                        />
                                </InputGroup>
                                {!this.tokenIsGiven ? (
                                    <Alert variant={"warning"}>
                                        Upišite aktivacijski kod!
                                    </Alert>
                                ) : null}
                            </Form.Group>
                            <InputGroup>
                                <Button
                                    variant="warning"
                                    onClick={this.callAction}
                                    className="btnSend"
                                >
                                    Aktiviraj
                                </Button>
                                <Button
                                    variant="danger"
                                    href={"prijava"}
                                    className="btnSend"
                                >
                                    Odustani
                                </Button>
                            </InputGroup>
                        </Card.Body>
                    </Card>
                </div>
            </div>

        );
    }

}
