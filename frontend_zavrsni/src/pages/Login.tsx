import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import "./design/signIn.css";
import {Alert, Card, InputGroup, ListGroup, OverlayTrigger, Popover, Spinner} from "react-bootstrap";
import jwt from "jwt-decode";
import {signin} from "../store/LoginService";
import logo_png from "../components/info/styles/background.png";
import {Icon} from "semantic-ui-react";

export class Login extends Component {

    constructor(props: Readonly<{}>) {
        super(props);
    }
    state = {
        user: "",
        password: "",
        numOfMistakes: 0,
        render:false,
        toHomePage: false
    };

    public notActivated = true;
    public sendingMail = true;

    componentDidMount(): void {
        sessionStorage.clear();
    }

    render() {
        const proper = { "overflowY": "hidden" ,backgroundImage: `url(${logo_png})` } as React.CSSProperties;
        if (this.state.toHomePage) {
            const item = sessionStorage.getItem("userInfo");
            if (item) {
                const token = jwt(item!);
                if (token) {
                    const confirmed = token.confirmed;
                    if(confirmed === true){
                        // @ts-ignore
                        const role = token.role;
                        if (role === "ADMIN") {
                            return <Redirect to="/pocetnaAdmin" />;
                        } else if (role === "CITIZEN") {
                            return <Redirect to="/pocetnaKorisnik" />;
                        } else {
                            return <h1>POGRESKA</h1>;
                        }
                    }
                    else{

                        this.sendingMail=true;
                        this.notActivated=false;

                    }
                } else {
                    console.log("token nije string");
                }
            } else {
                console.log("ne postoji item");
            }
        }
        return (
            <div className="AppSignIn" style={ proper}>
                <div className="AppSignIn-content">

                    <Card className="CardGenerator" bg="success">
                        <Card.Header className={"CardTitleAppSignIn"}>Prijava</Card.Header>
                        <Card.Body className={"CardBodyAppSignIn"}>
                            <Form.Group controlId="formUsername" className="passwordInputAppSignIn">
                                <Form.Label>Nadimak</Form.Label>
                                <InputGroup className="Title">
                                    <Form.Control
                                        autoComplete="off"
                                        type="username"
                                        placeholder="Nadimak"
                                        name="user"
                                        value={this.state.user}
                                        onChange={this.settingUser}
                                    />
                                </InputGroup>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword" className="passwordInputAppSignIn">
                                <Form.Label>Lozinka</Form.Label>
                                <Form.Control
                                    autoComplete="off"
                                    type="password"
                                    placeholder="Lozinka"
                                    name="password"
                                    value={this.state.password}
                                    onChange={this.settingPassword}
                                />
                            </Form.Group>

                            <InputGroup className="inputGroupAppSignIn">

                                {!this.sendingMail?(
                                    <Button variant="primary" disabled>
                                        <Spinner
                                            as="span"
                                            animation="border"
                                            size="sm"
                                            role="status"
                                            aria-hidden="true"
                                        />
                                        <span className="sr-only">Loading...</span>
                                    </Button>
                                ):(

                                    <Button
                                        className={"AppSignInButtons"}
                                        variant="success"
                                        onClick={this.callAction}
                                    >
                                        Prijava
                                    </Button>
                                )}

                                <Button
                                    className={"AppSignInQuitButton"}
                                    href={"/pocetna"}
                                >
                                    Odustani
                                </Button>

                            </InputGroup>


                            <InputGroup className="inputGroupAppSignIn">
                                <Link to="/registracija" className={"link"}>
                                    {"Nemate račun? Registrirajte se"}
                                </Link>
                            </InputGroup>

                            <InputGroup className="inputGroupAppSignIn">
                                <Link to="/zaboravljenaLozinka" className={"link"}>
                                    {"Zaboravili ste lozinku?"}
                                </Link>
                            </InputGroup>

                            <InputGroup className="inputGroupAppSignIn">
                                <Link to="/zaboravljenNadimak" className={"link"}>
                                    {"Zaboravili ste nadimak?"}
                                </Link>
                            </InputGroup>

                        </Card.Body>


                    {this.state.numOfMistakes > 0 ? (
                        <Alert variant={"danger"} className="alertInput">
                            Neispravna lozinka ili korisnicko ime!
                        </Alert>
                    ) : null}
                    {!this.notActivated ? (
                        <Alert variant={"danger"} className="alertInput">
                            Niste aktivirali račun, provjerite mail!
                        </Alert>
                    ) : null}
                    </Card>

                </div>
            </div>
        );
    }

    callAction = (dispatch: any) => {
        const credentials: any = {
            username: this.state.user,
            password: this.state.password
        };
        this.sendingMail=false;
        this.setState({render: false});
        signin(credentials)
            .then((res: { status: number }) => {
                if (res.status === 200) {
                    this.setState({ ...this.state, toHomePage: true });
                } else {
                    this.sendingMail=true;
                    this.setState({
                        ...this.state,
                        numOfMistakes: this.state.numOfMistakes + 1
                    });
                }
            })
            .catch(function(error: any) {
                console.log(error);
            });
    };

    private settingUser = (event: any) => {
        const text = event.target.value;
        this.setState({ ...this.state, user: text });
    };

    private settingPassword = (event: any) => {
        const text = event.target.value;
        this.setState({ ...this.state, password: text });
    };

}
