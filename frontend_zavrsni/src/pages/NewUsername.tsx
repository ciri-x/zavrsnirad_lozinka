import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import "./design/NewPassword.css";
import {Redirect } from "react-router-dom";
// @ts-ignore
import logo_png from "../components/info/styles/background.png";
import {Alert, Button, Card, ListGroup, OverlayTrigger, Popover, ProgressBar, Tooltip} from "react-bootstrap";
import InputGroup from "react-bootstrap/InputGroup";
import {confirmUsernameChange} from "../store/LoginService";
import ReactHtmlParser from "react-html-parser";
import {getPasswordStrength} from "../store/PasswordTesterService";
import {Icon} from "semantic-ui-react";


export class NewUsername extends Component {
    state = {
        render:false,
        citizen: true,
        token: "",
        newUsername: ""
    };

    private TokenAndUsernameIsGiven = true;
    private goToLogin = false;
    private unknownToken = true;
    private usernameExists=false;


    private callAction = (dispatch: any) => {
        if(this.state.token.length===0 || this.state.newUsername.length===0){
            this.TokenAndUsernameIsGiven = false;
            setTimeout(() => {
                this.TokenAndUsernameIsGiven = true; this.setState({render: false})
            }, 3000);
        }
        else{
            this.TokenAndUsernameIsGiven = true;
        }

        this.sendMailResetUsername(this.state.token, this.state.newUsername);



    };

    private sendMailResetUsername = (
        token:any,
        newUsername: any
    ) => {
        if(this.TokenAndUsernameIsGiven) {
            confirmUsernameChange(token, newUsername)
                .then(res => {
                    if (res === true) {
                        this.goToLogin = true;
                        this.setState({ ...this.state });
                    }
                    else{
                        this.usernameExists = true;
                        this.setState({ ...this.state });
                    }
                })
                .catch(error => {
                    this.unknownToken = false;
                    setTimeout(() => {
                        this.unknownToken = true; this.setState({render: false})
                    }, 3000);
                    this.setState({ ...this.state });
                    console.log(error);
                });
        }
        else{
            this.setState({ ...this.state });
        }
    };


    onChange = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {

        if (this.goToLogin) {
            return <Redirect to="/prijava" />;
        }
        const proper = { "overflowY": "hidden" ,backgroundImage: `url(${logo_png})` } as React.CSSProperties;
        const htmlTextNewPass =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:2.5vh;color: white;font-weight: bold">Primili ste mail sa aktivacijskim kodom! </p>\n' +
            "\n" +
            "        </div>\n" +
            "    </div>";
        return (
            <div className="AppNewPassword" style={ proper}>
                <div className="AppNewPassword-content">

                    <Card  className="CardNewPass" bg="success">
                        <Card.Header className={"CardTitle"}>Novi nadimak</Card.Header>
                        <div className={"TextNewPass"}>
                            {ReactHtmlParser(htmlTextNewPass)}
                        </div>
                        <Card.Body className={"CardBodyNewPass"}>
                            <Form.Group controlId="formTitle1" className="mailInput">
                                <InputGroup className="Title">
                                    <InputGroup.Text> Aktivacijski Kod </InputGroup.Text>
                                    <Form.Control
                                        className={"inputField"}
                                        autoComplete="off"
                                        type="text"
                                        placeholder="Upišite token koji ste dobili u poruci"
                                        aria-describedby="inputGroupPrepend"
                                        name="token"
                                        value={this.state.token}
                                        onChange={this.onChange}
                                    />
                                </InputGroup>
                            </Form.Group>
                            <InputGroup className="alertInput">
                                {!this.unknownToken ? (
                                    <Alert variant={"danger"}>Kod neispravan ili nadimak već postoji!</Alert>
                                ) : null}
                                {this.usernameExists ? (
                                    <Alert variant={"warning"}>Ovaj nadimak već postoji</Alert>
                                ) : null}
                            </InputGroup>
                            <Form.Group controlId="formTitle2" className="mailInput">
                                <InputGroup className="Title">
                                    <InputGroup.Text>Nadimak</InputGroup.Text>

                                        <Form.Control
                                            className={"inputField"}
                                            autoComplete="off"
                                            type="text"
                                            placeholder="Upišite novi nadimak"
                                            aria-describedby="inputGroupPrepend"
                                            name="newUsername"
                                            value={this.state.newUsername}
                                            onChange={this.onChange}
                                        />

                                </InputGroup >
                            </Form.Group>

                            <InputGroup className="alertInput">
                                {!this.TokenAndUsernameIsGiven ? (
                                    <Alert variant={"warning"} >
                                        Ispunite sva polja!
                                    </Alert>
                                ) : null}
                            </InputGroup>


                            <InputGroup>
                                <Button
                                    variant="warning"
                                    onClick={this.callAction}
                                    className="btnSend"
                                >
                                    Pošalji
                                </Button>
                                <Button
                                    variant="danger"
                                    href={"prijava"}
                                    className="btnSend"
                                >
                                    Odustani
                                </Button>
                            </InputGroup>
                        </Card.Body>
                    </Card>

                </div>
            </div>

        );
    }

}
