import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import "./design/NewPassword.css";
import {Redirect } from "react-router-dom";
// @ts-ignore
import logo_png from "../components/info/styles/background.png";
import {Alert, Button, Card, ListGroup, OverlayTrigger, Popover, ProgressBar, Tooltip} from "react-bootstrap";
import InputGroup from "react-bootstrap/InputGroup";
import {resetPass, confirmChange} from "../store/LoginService";
import ReactHtmlParser from "react-html-parser";
import {getPasswordStrength} from "../store/PasswordTesterService";
import {Icon} from "semantic-ui-react";


export class NewPassword extends Component {
    state = {
        isLonger: false,
        isLower: false,
        isUpper: false,
        isDigit: false,
        isPunct: false,
        progress: 0,
        render:false,
        citizen: true,
        token: "",
        newPassword: ""
    };

    private TokenAndPassIsGiven = true;
    private goToLogin = false;
    public passwordSlab = true;
    private unknownToken = true;


    private testPassword = (
        password: any
    ) => {
        getPasswordStrength(password)
            .then(res => {
                if (res.status === 200) {
                    this.updateTest(res.data);
                }
            })
            .catch(error => {
                console.log(error);
            });
    };

    private updateTest = (
        data: any
    ) => {
        this.setState({progress: data[0]});
        this.formattingParameters(data[1]);
    }

    private formattingParameters = (
        parameters: any[]
    ) => {
        Object.values(parameters).map((val,k)=>
            {switch(k){
                case 0:
                    this.setState({isUpper:val});
                    break;
                case 1:
                    this.setState({isDigit:val});
                    break;
                case 2:
                    this.setState({isPunct:val});
                    break;
                case 3:
                    this.setState({isLower:val});
                    break;
                case 4:
                    this.setState({isLonger:val});
                    break;
            }
            }
        )
    }



    private callAction = (dispatch: any) => {
        if(this.state.token.length===0 || this.state.newPassword.length===0){
            this.TokenAndPassIsGiven = false;
            setTimeout(() => {
                this.TokenAndPassIsGiven = true; this.setState({render: false})
            }, 3000);
        }
        else{
            this.TokenAndPassIsGiven = true;
        }
        if (this.state.progress < 100) {
            this.passwordSlab = false;
            setTimeout(() => {
                this.passwordSlab = true; this.setState({render: false})
            }, 3000);
            this.sendMailResetPassword(this.state.token, this.state.newPassword);
        } else {
            this.passwordSlab = true;
            this.sendMailResetPassword(this.state.token, this.state.newPassword);
        }


    };

    private sendMailResetPassword = (
        token:any,
        newPassword: any
    ) => {
        if(this.TokenAndPassIsGiven&&this.passwordSlab) {
            confirmChange(token, newPassword)
                .then(res => {
                    if (res === true) {
                        this.goToLogin = true;
                        this.setState({ ...this.state });
                    }
                })
                .catch(error => {
                    this.unknownToken = false;
                    setTimeout(() => {
                        this.unknownToken = true; this.setState({render: false})
                    }, 3000);
                    this.setState({ ...this.state });
                    console.log(error);
                });
        }
        else{
            this.setState({ ...this.state });
        }
    };


    onChange = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    onChangePass = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
        this.testPassword(e.target.value);
    }

    render() {

        if (this.goToLogin) {
            return <Redirect to="/prijava" />;
        }
        const proper = { "overflowY": "hidden" ,backgroundImage: `url(${logo_png})` } as React.CSSProperties;
        const htmlTextNewPass =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:2.5vh;color: white;font-weight: bold">Primili ste mail sa aktivacijskim kodom! </p>\n' +
            "\n" +
            "        </div>\n" +
            "    </div>";
        return (
            <div className="AppNewPassword" style={ proper}>
                <div className="AppNewPassword-content">

                        <Card  className="CardNewPass" bg="success">
                            <Card.Header className={"CardTitle"}>Nova Lozinka</Card.Header>
                            <div className={"TextNewPass"}>
                                {ReactHtmlParser(htmlTextNewPass)}
                            </div>
                            <Card.Body className={"CardBodyNewPass"}>
                                <Form.Group controlId="formTitle1" className="mailInput">
                                    <InputGroup className="Title">
                                        <InputGroup.Text> Aktivacijski Kod </InputGroup.Text>
                                            <Form.Control
                                                className={"inputField"}
                                                autoComplete="off"
                                                type="text"
                                                placeholder="Upišite token koji ste dobili u poruci"
                                                aria-describedby="inputGroupPrepend"
                                                name="token"
                                                value={this.state.token}
                                                onChange={this.onChange}
                                            />
                                    </InputGroup>
                                </Form.Group>
                                <InputGroup className="alertInput">
                                    {!this.unknownToken ? (
                                        <Alert variant={"danger"}>Kod neispravan!</Alert>
                                    ) : null}
                                </InputGroup>
                                <Form.Group controlId="formTitle2" className="mailInput">
                                    <InputGroup className="Title">
                                    <InputGroup.Text>Lozinka</InputGroup.Text>
                                        <OverlayTrigger
                                            key={"testedPassword"}
                                            placement={"right-start"}
                                            overlay={
                                                <Popover id={`popover-basic`} className = {"tooltipAppSignIn"}>
                                                    <Popover.Title as="h3">Lozinka treba sadržavati:</Popover.Title>
                                                    <Popover.Content >
                                                        <ListGroup as="ul" className={"popoverContent"}>
                                                            {!this.state.isLonger ?
                                                                <ListGroup.Item variant={"danger"}>Više od 8 znakova <Icon name="close" /></ListGroup.Item>
                                                                :
                                                                (<ListGroup.Item variant={"success"}>Više od 8 znakova <Icon name="check" /></ListGroup.Item>)
                                                            }
                                                            {!this.state.isPunct ?
                                                                <ListGroup.Item variant={"danger"}>Specijalni znak <Icon name="close" /></ListGroup.Item>
                                                                :
                                                                (<ListGroup.Item variant={"success"}>Specijalni znak <Icon name="check" /></ListGroup.Item>)
                                                            }
                                                            {!this.state.isDigit ?
                                                                <ListGroup.Item variant={"danger"}>Broj <Icon name="close" /></ListGroup.Item>
                                                                :
                                                                (<ListGroup.Item variant={"success"}>Broj <Icon name="check" /></ListGroup.Item>)
                                                            }
                                                            {!(this.state.isUpper && this.state.isLower) ?
                                                                <ListGroup.Item variant={"danger"}>Velika i mala slova <Icon name="close" /></ListGroup.Item>
                                                                :
                                                                (<ListGroup.Item variant={"success"}>Veliko i mala slova <Icon name="check" /></ListGroup.Item>)
                                                            }
                                                        </ListGroup>
                                                    </Popover.Content>
                                                </Popover>
                                            }
                                        >
                                        <Form.Control
                                            className={"inputField"}
                                            autoComplete="off"
                                            type="password"
                                            placeholder="Upišite novu lozinku"
                                            aria-describedby="inputGroupPrepend"
                                            name="newPassword"
                                            value={this.state.newPassword}
                                            onChange={this.onChangePass}
                                        />
                                        </OverlayTrigger>
                                </InputGroup >
                                </Form.Group>
                                <InputGroup className="alertInput">
                                    {!this.passwordSlab ? (
                                        <Alert variant={"danger"}>Loša lozinka!</Alert>
                                    ) : null}
                                </InputGroup>
                                <InputGroup className="alertInput">
                                    {!this.TokenAndPassIsGiven ? (
                                        <Alert variant={"warning"} >
                                            Ispunite sva polja!
                                        </Alert>
                                    ) : null}
                                </InputGroup>


                                <InputGroup>
                                    <Button
                                        variant="warning"
                                        onClick={this.callAction}
                                        className="btnSend"
                                    >
                                        Pošalji
                                    </Button>
                                    <Button
                                        variant="danger"
                                        href={"prijava"}
                                        className="btnSend"
                                    >
                                        Odustani
                                    </Button>
                                </InputGroup>
                            </Card.Body>
                        </Card>

                </div>
            </div>

        );
    }

}
