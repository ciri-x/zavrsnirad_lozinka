import React, { Component } from "react";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { Switch, Route, Redirect } from "react-router-dom";
import jwt from "jwt-decode";
import logo from "../components/info/styles/logoSmall.png";
import Korisnici from "../components/forAdmin/UserList";
import Info from "../components/info/Info";


export class PageForAdmin extends Component {
    state = {
        odjava: false
    };

    render() {

        if (this.state.odjava) {
            return <Redirect to={"/"} />;
        }
        const item = sessionStorage.getItem("userInfo");
        if (item) {
            const token = jwt(item!);
            if (token) {
                // @ts-ignore
                const role = token.role;
                if (role !== "ADMIN") {
                    return <Redirect to="/" />;
                }
            }
        } else {
            return <Redirect to="/" />;
        }
        return (
            <div>
                <Navbar className={"NavLozinka"} sticky={"top"} collapseOnSelect expand="lg">
                    <Navbar.Brand href="/pocetnaAdmin/">
                        <img
                            src={logo}
                            width="120"
                            height="40"
                            alt="logo"
                            className="d-inline-block align-top"
                        />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="NavLozinka">
                            <Nav.Link href="/pocetnaAdmin">Home</Nav.Link>
                            <Nav.Link href="/pocetnaAdmin/korisnici">Popis korisnika</Nav.Link>
                            <Nav.Link eventKey={2} onClick={this.odjava}>
                                <button>Odjava</button>
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                <Switch>
                    <Route path="/pocetnaAdmin/korisnici" component={Korisnici} />
                    <Route path="/pocetnaAdmin" component={Info} />
                </Switch>
            </div>
        );
    }

    odjava = () => {
        sessionStorage.clear();
        this.setState({ odjava: true });
    };
}
