import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import "./design/signIn.css";
import {Alert, Card, InputGroup, ListGroup, OverlayTrigger, Popover, Spinner, Tooltip} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import {register} from "../store/LoginService";
import { Link, Redirect } from "react-router-dom";
import logo_png from "../components/info/styles/background.png";
import {Icon} from "semantic-ui-react";
import {getPasswordStrength} from "../store/PasswordTesterService";



export class Register extends Component {
    private goToConfirmMail = false;

    state = {
        username: "",
        password: "",
        name: "",
        surname: "",
        email: "",
        isLower: false,
        isUpper: false,
        isDigit: false,
        isPunct: false,
        progress: 0,
        render:false,
        isLonger: false
    };

    public svaPoljaIspunjena = true;
    public passwordSlab = true;
    public ispravanJeMail = true;
    public postojiUsername = false;
    public sendingMail = true;


    private testPassword = (
        password: any
    ) => {
        getPasswordStrength(password)
            .then(res => {
                if (res.status === 200) {
                    this.updateTest(res.data);
                }
            })
            .catch(error => {
                console.log(error);
            });
    };

    private updateTest = (
        data: any
    ) => {
        this.setState({progress: data[0]});
        this.formattingParameters(data[1]);
    }


    private formattingParameters = (
        parameters: any[]
    ) => {
        Object.values(parameters).map((val,k)=>
            {switch(k){
                case 0:
                    this.setState({isUpper:val});
                    break;
                case 1:
                    this.setState({isDigit:val});
                    break;
                case 2:
                    this.setState({isPunct:val});
                    break;
                case 3:
                    this.setState({isLower:val});
                    break;
                case 4:
                    this.setState({isLonger:val});
                    break;
            }
            }
        )
    }

    private callAction = (dispatch: any) => {
        const user = {
            username: this.state.username,
            password: this.state.password,
            firstName: this.state.name,
            lastName: this.state.surname,
            email: this.state.email,
            role: "CITIZEN"
        };
        if (
            !(
                this.state.username &&
                this.state.email &&
                this.state.surname &&
                this.state.name &&
                this.state.password
            )
        ) {
            this.svaPoljaIspunjena = false;
            setTimeout(() => {
                this.svaPoljaIspunjena = true; this.setState({render: false})
            }, 3000);
        } else {
            this.svaPoljaIspunjena = true;
        }

        if (this.state.email.includes("@")) {
            this.ispravanJeMail = true;
        } else {
            this.ispravanJeMail = false;
            setTimeout(() => {
                this.ispravanJeMail = true; this.setState({render: false})
            }, 3000);
        }
        if (this.state.progress < 100) {
            this.passwordSlab = false;
            setTimeout(() => {
                this.passwordSlab = true; this.setState({render: false})
            }, 3000);
            this.callingService(user);
        } else {
            this.passwordSlab = true;
            this.callingService(user);
        }
    };

    private callingService = (user: any) => {
        if (
            this.svaPoljaIspunjena &&
            this.passwordSlab &&
            this.ispravanJeMail
        ) {
            this.sendingMail=false;
            this.setState({render: false});
            register(user)
                .then(res => {
                    if (res === true) {
                        this.goToConfirmMail = true;
                        this.setState({ ...this.state });
                    } else {
                        this.postojiUsername = true;
                        this.sendingMail=true;
                        this.setState({render: false});
                        this.setState({ ...this.state });
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        } else {
            this.setState({ ...this.state });
        }
    };
    private changeState = (event: any) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value });
    };

    onChange = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
        this.testPassword(e.target.value);
    }

    render() {
        if (this.goToConfirmMail) {
            return <Redirect to="/potvrdaRegistracije" />;
        }
        const proper = { "overflowY": "hidden" ,backgroundImage: `url(${logo_png})` } as React.CSSProperties;
        return (
            <div className="AppSignIn" style={ proper}>
                <div className="AppSignIn-content">
                    <Card className="CardGenerator" bg="success">
                        <Card.Header className={"CardTitleAppSignIn"}>Registracija</Card.Header>
                        <Card.Body className={"CardBodyAppSignIn"}>
                            <Form.Group controlId="formUsername" className="passwordInputAppSignIn">
                                <Form.Label>Nadimak</Form.Label>
                                <InputGroup className="Title">
                                    <Form.Control
                                        type="username"
                                        autoComplete="off"
                                        placeholder="Unesite nadimak"
                                        name={"username"}
                                        onChange={this.changeState}
                                        value={this.state.username}
                                    />
                                </InputGroup>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword"  className="passwordInputAppSignIn">
                                <Form.Label>Lozinka</Form.Label>
                                <InputGroup className="Title">
                                    <OverlayTrigger
                                        key={"testedPassword"}
                                        placement={"right-start"}
                                        overlay={
                                            <Popover id={`popover-basic`} className = {"tooltipAppSignIn"}>
                                                <Popover.Title as="h3">Lozinka treba sadržavati:</Popover.Title>
                                                <Popover.Content >
                                                    <ListGroup as="ul" className={"popoverContent"}>
                                                        {!this.state.isLonger ?
                                                            <ListGroup.Item variant={"danger"}>Više od 8 znakova <Icon name="close" /></ListGroup.Item>
                                                            :
                                                            (<ListGroup.Item variant={"success"}>Više od 8 znakova <Icon name="check" /></ListGroup.Item>)
                                                        }
                                                        {!this.state.isPunct ?
                                                            <ListGroup.Item variant={"danger"}>Specijalni znak <Icon name="close" /></ListGroup.Item>
                                                            :
                                                            (<ListGroup.Item variant={"success"}>Specijalni znak <Icon name="check" /></ListGroup.Item>)
                                                        }
                                                        {!this.state.isDigit ?
                                                            <ListGroup.Item variant={"danger"}>Broj <Icon name="close" /></ListGroup.Item>
                                                            :
                                                            (<ListGroup.Item variant={"success"}>Broj <Icon name="check" /></ListGroup.Item>)
                                                        }
                                                        {!(this.state.isUpper && this.state.isLower) ?
                                                            <ListGroup.Item variant={"danger"}>Velika i mala slova <Icon name="close" /></ListGroup.Item>
                                                            :
                                                            (<ListGroup.Item variant={"success"}>Veliko i mala slova <Icon name="check" /></ListGroup.Item>)
                                                        }
                                                    </ListGroup>
                                                </Popover.Content>
                                            </Popover>
                                        }
                                    >
                                    <Form.Control
                                        type="password"
                                        autoComplete="off"
                                        placeholder="Lozinka"
                                        name={"password"}
                                        onChange={this.onChange}
                                        value={this.state.password}
                                    />
                                </OverlayTrigger>
                                </InputGroup>
                                <InputGroup className="alertInput">
                                    {!this.passwordSlab ? (
                                        <Alert variant={"danger"}>Loša lozinka!</Alert>
                                    ) : null}
                                </InputGroup>
                            </Form.Group>




                            <Form.Group controlId="formName"  className="passwordInputAppSignIn">
                                <Form.Label>Ime</Form.Label>
                                <Form.Control
                                    autoComplete="off"
                                    type="username"
                                    placeholder="Ime"
                                    name={"name"}
                                    onChange={this.changeState}
                                    value={this.state.name}
                                />
                            </Form.Group>

                            <Form.Group controlId="formSurname"  className="passwordInputAppSignIn">
                                <Form.Label>Prezime</Form.Label>
                                <Form.Control
                                    autoComplete="off"
                                    type="Surname"
                                    placeholder="Prezime"
                                    name={"surname"}
                                    onChange={this.changeState}
                                    value={this.state.surname}
                                />
                            </Form.Group>

                            <Form.Group controlId="formEmail"  className="passwordInputAppSignIn">
                                <Form.Label>Email</Form.Label>
                                <Form.Control
                                    autoComplete="off"
                                    type="Email"
                                    placeholder="Email"
                                    name={"email"}
                                    onChange={this.changeState}
                                    value={this.state.email}
                                />
                                <InputGroup className="alertInput">
                                    {!this.ispravanJeMail ? (
                                        <Alert variant={"danger"}>nije ispravan mail</Alert>
                                    ) : null}
                                </InputGroup>
                            </Form.Group>



                            <InputGroup className="inputGroupAppSignIn">

                                {!this.sendingMail?(
                                    <Button variant="primary" disabled>
                                        <Spinner
                                            as="span"
                                            animation="border"
                                            size="sm"
                                            role="status"
                                            aria-hidden="true"
                                        />
                                        <span className="sr-only">Loading...</span>
                                    </Button>
                                ):(
                                    <Button
                                        className={"AppSignInButtons"}
                                        onClick={this.callAction}
                                    >
                                        Registriraj se
                                    </Button>
                                )}

                                <Button
                                    className={"AppSignInQuitButton"}
                                    href={"/pocetna"}
                                >
                                    Odustani
                                </Button>

                            </InputGroup>

                            <InputGroup className="alertInput">
                                {this.postojiUsername ? (
                                    <Alert variant={"warning"}>Postoji username ili email adresa</Alert>
                                ) : null}
                                {!this.svaPoljaIspunjena ? (
                                    <Alert variant={"warning"}>Nisu sva polja ispunjena</Alert>
                                ) : null}
                            </InputGroup>

                            <InputGroup className="inputGroupAppSignIn">
                                <Link to="/prijava" className={"link"}>{"Imate račun? Ulogirajte se"}</Link>
                            </InputGroup>
                        </Card.Body>

                    </Card>
                </div>
            </div>
        );
    }

}
