import React, { Component } from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import logo from "../components/info/styles/logoSmall.png"
import { Route, Switch } from "react-router-dom";
import "./design/FirstPage.css"
import Info from "../components/info/Info";
import About from "../components/info/AboutPage"
import Tester from "../components/forUser/PasswordTester";
import Quiz from "../components/forRegistration/PasswordQuiz"
import Generator from "../components/forRegistration/PasswordGenerator"


export class FirstPage extends Component {
    render() {
        return (
            <div >
                <Navbar className={"NavLozinka"} sticky={"top"} collapseOnSelect expand="lg" >
                    <Navbar.Brand href="/pocetna">
                        <img
                            src={logo}
                            width="120"
                            height="40"
                            alt="logo"
                            className="d-inline-block align-top"
                        />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav" >
                        <Nav className="NavLozinka mr-auto">
                            <Nav.Link href="/pocetna/kviz">Kviz</Nav.Link>
                            <Nav.Link href="/pocetna/generator">Generator</Nav.Link>
                            <Nav.Link href="/pocetna/tester">Tester</Nav.Link>
                            <Nav.Link href="/pocetna/informacije">O nama</Nav.Link>
                        </Nav>
                        <Nav className="NavLozinka mr-auto">
                            <Nav.Link href="/prijava"><button>Prijava</button></Nav.Link>
                            <Nav.Link href="/registracija"><button>Registracija</button></Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <Switch>
                    <Route path="/pocetna/tester" component={Tester} />
                    <Route path="/pocetna/generator" component={Generator} />
                    <Route path="/pocetna/kviz" component={Quiz} />
                    <Route path="/pocetna/informacije" component={About} />
                    <Route path="/pocetna" component={Info} />
                </Switch>
            </div>

        );
    }
}

