import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import {Login} from "./pages/Login";
import {Register} from "./pages/Register";
import {ResetPassword} from "./pages/ResetPassword"
import {NewPassword} from "./pages/NewPassword"
import {ResetUsername} from "./pages/ResetUsername"
import {NewUsername} from "./pages/NewUsername"
import {Confirm} from "./pages/Confirm"
import {PageForUser} from "./pages/PageForUser";
import {PageForAdmin} from "./pages/PageForAdmin";
import {FirstPage} from "./pages/FirstPage";
import {Redirect} from "react-router-dom";


function App() {

    return (
        <Router>
            <Switch>
                <Route path="/pocetna">
                    <FirstPage />
                </Route>
                <Route exact path="/prijava">
                    <Login />
                </Route>
                <Route exact path="/registracija">
                    <Register />
                </Route>
                <Route exact path="/zaboravljenaLozinka">
                    <ResetPassword />
                </Route>
                <Route exact path="/promjenaLozinke">
                    <NewPassword />
                </Route>
                <Route exact path="/zaboravljenNadimak">
                    <ResetUsername />
                </Route>
                <Route exact path="/promjenaNadimka">
                    <NewUsername />
                </Route>
                <Route exact path="/potvrdaRegistracije">
                    <Confirm />
                </Route>
                <Route path="/pocetnaAdmin">
                    <PageForAdmin />
                </Route>
                <Route path="/pocetnaKorisnik">
                    <PageForUser />
                </Route>
                <Route path="/">
                    <Redirect to="/pocetna" />
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
