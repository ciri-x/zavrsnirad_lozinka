import React from "react";
import "./styles/infoStyles.css";
import ReactHtmlParser from "react-html-parser";
import background_png from "./styles/background.png";
import logo_png from "./styles/transparentSmall.png"
import {Card} from "react-bootstrap";


export default class AboutPage extends React.Component {
    render() {

        const proper = { "overflowY": "hidden" ,backgroundImage: `url(${background_png})` } as React.CSSProperties;
        const htmlBody =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            '            <p style="text-align:center;font-size:2vh;font-style:italic;color: black;font-weight: bold"><br/>Aplikacija je razvijena u okviru završnog rada:<br/> Marko Cirimotić, Programsko rješenje za\n' +
            "zaštitu djece<br/> u digitalnom okruženju zasnovano na tehnologijama weba.\n" +
            "\n" +
            "<br/>Sveučilište u Zagrebu Fakultet elektrotehnike i računarstva,<br/> ak. godina 2019/2020.\n" +
            "<br/>Mentorica: prof. dr.sc. Željka Car<br/> Asistentica: dr. sc. Ivana Rašan</p>\n" +
            "\n" +
            '<p style="text-align:center;font-size:2vh;font-style:italic;color: black;font-weight: bold">Iako danas živimo u dobu bez ratnih sukoba,\n' +
            "<br/>javlja se strah od napada u digitalnom svijetu<br/> u bilo kojem segmentu (poslovanje, <br/>servisi za plaćanje, trgovina, osobne aplikacije i sl.),\n" +
            "<br/>a osobito je strah prisutan kada se radi o djeci<br/> u digitalnom svijetu te se nerijetko<br/> postavlja pitanje kako ih zaštititi.</p>\n" +
            "\n" +
            '            <p style="text-align:center;font-size:2vh;font-style:italic;color: black;font-weight: bold">\n' +
            "<br/>Pokretanje ove aplikacije će Vašoj djeci<br/> pružiti mogućnost učenja temeljnih pravila<br/> vezanih uz stvaranje snažne lozinke<br/> kao i korištenje tog znanja.</p>\n" +
            "\n" +
            '            <p style="text-align:center;font-size:2vh;color: black;font-weight: bold">e-mail: zavrsni.lozinka@gmail.com<br/>\n' +
            "               </p>\n" +
            "\n" +
            "        </div>\n" +
            "    </div>";


        return (
            <div className="App" style={ proper}>
                <div className="App-content">
                        <Card className="CardAbout">
                                <Card.Title className={"cardTitle"}>
                                    <img
                                        className={"logoImg"}
                                        src={logo_png}
                                        alt=""
                                    />
                                </Card.Title>
                                <Card.Text>
                                    {ReactHtmlParser(htmlBody)}
                                </Card.Text>
                        </Card>
                </div>
            </div>

        );
    }
}
