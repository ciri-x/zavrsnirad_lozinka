import React from "react";
import "./styles/infoStyles.css";
import ReactHtmlParser from "react-html-parser";
import background_png from "./styles/background.png";
import {Button, Card, Carousel} from "react-bootstrap";
import logo_png from "./styles/transparentSmall.png";


export default class Info extends React.Component {
    render() {

        const proper = { "overflowY": "hidden" ,backgroundImage: `url(${background_png})` } as React.CSSProperties;

        const htmlAbout =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:3vh;color: white;font-style:italic;font-weight: bold">Sigurnost? Što je danas sigurnost?<br/></p>\n' +
            "\n" +
            '            <p style="text-align:center;font-size:2vh;color: white;font-weight: bold">Ako gledate ovu aplikaciju, sigurno se i Vi to pitate<br/> pa ćemo pokušati dati odgovor na Vaše pitanje.</p>' +
            "\n" +
            '            <p style="text-align:center;font-size:2vh;color: white;font-weight: bold">Svjesni smo da se potpuna sigurnost ne može postići,<br/> ali ono što možemo napraviti je to<br/> da učimo i naučimo ono što možemo.</p>\n' +
            "        </div>\n" +
            "    </div>";
        const htmlTextGenerator =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:4vh;font-style:italic;color: white;font-weight: bold">Želite saznati savršenu lozinku<br/> upravo za vas?</p>\n' +
            "\n" +
            "        </div>\n" +
            "    </div>";

        const htmlTextTester =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:4vh;font-style:italic;color: white;font-weight: bold">Niste sigurni je li vaša<br/> lozinka dovoljno jaka?</p>\n' +
            "\n" +
            '            <p style="text-align:center;font-size:4vh;font-style:italic;color: white;font-weight: bold">Provjerite to u što kraćem roku<br/> našim testerom!</p>\n' +
            "\n" +
            "        </div>\n" +
            "    </div>";

        const htmlTextKviz =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:4vh;font-style:italic;color: white;font-weight: bold">Mislite da dobro pamtite svoje lozinke?</p>\n' +
            "\n" +
            '            <p style="text-align:center;font-size:4vh;font-style:italic;color: white;font-weight: bold">Testirajte svoje znanje<br/> našim kvizom!</p>\n' +
            "\n" +
            "        </div>\n" +
            "    </div>";

        return (
            <div className="App" style={ proper}>
                <div className="Logo-App">
                    <img
                        className={"logoImgInfo"}
                        src={logo_png}
                        alt=""
                    />
                </div>

                <div className="App-content">
                    <Carousel
                        className={"Carousel"}
                        pauseOnHover={true}
                        interval={7000}
                    >
                        <Carousel.Item>
                            <div className={"CarDesign"}>
                                {ReactHtmlParser(htmlAbout)}
                            </div>
                            <div className={"CarDesign"}>
                                <Button
                                    variant="warning"
                                    href={"/registracija"}
                                    className="btnHomePage"
                                >
                                    Registracija
                                </Button>
                                <Button
                                    variant="warning"
                                    href={"/pocetna/informacije"}
                                    className="btnHomePage"
                                >
                                    Više...
                                </Button>
                            </div>
                        </Carousel.Item>

                        <Carousel.Item>
                            <div className={"CarDesign"}>
                                {ReactHtmlParser(htmlTextGenerator)}
                            </div>
                            <div className={"CarDesign"}>
                                <Button
                                    variant="warning"
                                    href={"/pocetna/generator"}
                                    className="btnHomePage"
                                >
                                    GENERATOR
                                </Button>
                            </div>
                        </Carousel.Item>
                        <Carousel.Item>
                            <div className={"CarDesign"}>
                                {ReactHtmlParser(htmlTextTester)}
                            </div>
                            <div className={"CarDesign"}>
                                <Button
                                    variant="warning"
                                    href={"/pocetna/tester"}
                                    className="btnHomePage"
                                >
                                    TESTER
                                </Button>
                            </div>
                        </Carousel.Item>
                        <Carousel.Item>
                            <div className={"CarDesign"}>
                                {ReactHtmlParser(htmlTextKviz)}
                            </div>
                            <div className={"CarDesign"}>
                                <Button
                                    variant="warning"
                                    href={"/pocetna/kviz"}
                                    className="btnHomePage"
                                >
                                    KVIZ
                                </Button>
                            </div>
                        </Carousel.Item>
                    </Carousel>
                </div>
            </div>

        );
    }
}
