import React, { Component } from "react";
import InputGroup from "react-bootstrap/InputGroup";
import {Alert, Button, Card, Form, OverlayTrigger, Tooltip} from "react-bootstrap";
import logo_png from "../info/styles/background.png";
import "../forUser/Styles/GeneratorStyles.css"
import ReactHtmlParser from "react-html-parser";
import {getGeneratedPasswordAuto, getGeneratedPasswordManual} from "../../store/PasswordGeneratorService";



export default class PasswordGenerator extends Component {

    state = {
        word: "",
        isUpper: false,
        isDigit: false,
        isPunct: false,
        strength: "good",
        showManual: false,
        generatedPass: "",
        render:false
    }

    private showPassword = false;
    private isWordGiven = true;
    private tooLongForWeak = true;
    private tooShortForStrong = true;



    private callAction = (dispatch: any) => {
        if(this.state.word.length===0){
            this.isWordGiven = false;
            setTimeout(() => {
                this.isWordGiven = true; this.setState({render: false})
            }, 3000);
        }
        else{
            this.isWordGiven = true;
        }
        if(this.state.word.length==1 && this.state.strength==="strong" && !this.state.showManual){
            this.tooShortForStrong = false;
            setTimeout(() => {
                this.tooShortForStrong = true; this.setState({render: false})
            }, 4000);

        }
        else{
            this.tooShortForStrong = true;
        }
        if(this.state.word.length>=8 && this.state.strength==="weak" && !this.state.showManual){
            this.tooLongForWeak = false;
            setTimeout(() => {
                this.tooLongForWeak = true; this.setState({render: false})
            }, 4000);

        }
        else{
            this.tooLongForWeak = true;
        }
        if(this.state.showManual){
            this.generatePassManual(this.state.word, this.state.isUpper,this.state.isDigit,this.state.isPunct);
        }
        else{
            this.generatePassAuto(this.state.word,this.state.strength);
        }

    };

    private generatePassManual = (
        word: any,
        isUpper: any,
        isDigit: any,
        isPunct: any
    ) => {
        if(this.isWordGiven) {
            getGeneratedPasswordManual(word, isUpper, isDigit, isPunct)
                .then(res => {
                    if (res.status === 200) {
                        this.showPassword = true;
                        this.setState({generatedPass: res.data});
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        }
        else{
            this.setState({ ...this.state });
        }
    };
    private generatePassAuto = (
        word: any,
        strength: any
    ) => {
        if(this.isWordGiven&&this.tooLongForWeak&&this.tooShortForStrong) {
            getGeneratedPasswordAuto(word, strength)
                .then(res => {
                    if (res.status === 200) {
                        this.showPassword = true;
                        this.setState({generatedPass: res.data});
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        }
        else{
            this.setState({ ...this.state });
        }
    };

    onChange = (e: any) => this.setState({ [e.target.name]: e.target.value });
    handleToggle = (e: any) => this.setState({
        [e.target.id]: e.target.checked
    });



    render() {
        const proper = { "overflowY": "hidden" ,backgroundImage: `url(${logo_png})` } as React.CSSProperties;

        const htmlTextGenerator =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:2.5vh;color: white;font-weight: bold">Generator lozinke će temeljem riječi koju unesete<br/> u polje \n' +
            "\n" +
            'generirati primjere jake, srednje jake ili slabe lozinke. </p>\n' +
            "        </div>\n" +
            "    </div>";

        const htmlRegisterGenerator =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:2.5vh;color: white;font-weight: bold">Želite iskoristiti generirane lozinke u kvizu?<br/>\n' +
            "\n" +
            'Registrirajte se pritiskom na gumb!</p>\n' +
            "        </div>\n" +
            "    </div>";


        return (
            <div className="AppGenerator" style={ proper}>
                <div className="AppGenerator-content">

                    <Card className="CardGenerator" bg="success">
                        <Card.Header className={"CardTitle"}>Generator lozinke</Card.Header>
                        <div className={"TextGenerator"}>
                            {ReactHtmlParser(htmlTextGenerator)}
                        </div>
                        <Card.Body className={"CardBodyGenerator"}>
                            <Form.Group controlId="formTitle"  className="passwordInput">
                                <InputGroup className="Title" >
                                    <InputGroup.Text>Lozinka</InputGroup.Text>
                                    <OverlayTrigger
                                        key={"good"}
                                        placement={"bottom"}
                                        overlay={
                                            <Tooltip id={`tooltip-bottom`}>
                                                Upisana riječ će se u potpunosti<br/>
                                                iskoristiti u lozinci.
                                            </Tooltip>
                                        }
                                    >
                                        <Form.Control
                                            className={"inputField"}
                                            autoComplete="off"
                                            type="text"
                                            placeholder="Pomoćna riječ"
                                            aria-describedby="inputGroupPrepend"
                                            maxLength={10}
                                            name="word"
                                            value={this.state.word}
                                            onChange={this.onChange}
                                        />
                                    </OverlayTrigger>
                                </InputGroup>
                                <InputGroup className="alertInputGenerator">
                                    {!this.isWordGiven ? (
                                        <Alert variant={"warning"}>
                                            Upišite neku pomoćnu riječ (bez korištenja brojeva i posebnih znakova)!
                                        </Alert>
                                    ) : null}
                                </InputGroup>
                                <InputGroup className="alertInputGenerator">
                                {!this.tooLongForWeak ? (
                                    <Alert variant={"warning"}>
                                        Ako želite slabu lozinku duljina riječi treba biti kraća od 8!
                                    </Alert>
                                ) : null}
                                {!this.tooShortForStrong ? (
                                        <Alert variant={"warning"}>
                                            Ako želite jaku lozinku trebate upisati barem 2 slova!
                                        </Alert>
                                    ) : null}

                                </InputGroup>
                                {!this.state.showManual ?
                                    <Form>
                                        <InputGroup className={"newButton"}>
                                            <OverlayTrigger
                                                key={"weak"}
                                                placement={"bottom"}
                                                overlay={
                                                    <Tooltip id={`tooltip-bottom`}>
                                                        Lozinka koja ne sadrži jedno od navedenog i kraća od 8 simbola: <br/>
                                                        - veliko i malo slovo <br/>
                                                        - broj<br/>
                                                        - specijalni znak
                                                    </Tooltip>
                                                }
                                            >
                                                <Form.Check
                                                    name = "strength"
                                                    inline label="Slaba"
                                                    type={"radio"}
                                                    id={"weak"}
                                                    value = "weak"
                                                    checked={this.state.strength==="weak"}
                                                    onChange={this.onChange}
                                                />
                                            </OverlayTrigger>
                                            <OverlayTrigger
                                                key={"good"}
                                                placement={"bottom"}
                                                overlay={
                                                    <Tooltip id={`tooltip-bottom`}>
                                                        Lozinka koja je ili duža od 8 simbola ili sadrži nešto od navedenog:<br/>
                                                        - veliko i malo slovo <br/>
                                                        - broj<br/>
                                                        - specijalni znak<br/>
                                                        Ali ne ispunjuje oba uvjeta!
                                                    </Tooltip>
                                                }
                                            >
                                                <Form.Check
                                                    name = "strength"
                                                    inline label="Srednja"
                                                    type={"radio"}
                                                    id={"good"}
                                                    value = "good"
                                                    defaultChecked
                                                    checked={this.state.strength==="good"}
                                                    onChange={this.onChange}
                                                />
                                            </OverlayTrigger>
                                            <OverlayTrigger
                                                key={"strong"}
                                                placement={"bottom"}
                                                overlay={
                                                    <Tooltip id={`tooltip-bottom`}>
                                                        Lozinka koja je duža od 8 simbola i sadrži sve od navedenog: <br/>
                                                        - veliko i malo slovo <br/>
                                                        - broj<br/>
                                                        - specijalni znak
                                                    </Tooltip>
                                                }
                                            >
                                                <Form.Check
                                                    name = "strength"
                                                    inline label="Jaka"
                                                    type={"radio"}
                                                    id={`strong`}
                                                    value = "strong"
                                                    checked={this.state.strength==="strong"}
                                                    onChange={this.onChange}
                                                />
                                            </OverlayTrigger>
                                        </InputGroup>
                                    </Form>

                                    :(
                                        <Form>
                                            <InputGroup >
                                                <Form.Check
                                                    className={"SwitchButtonGenerator"}
                                                    type="switch"
                                                    id="isUpper"
                                                    label="Velika i mala slova?"

                                                    checked={this.state.isUpper}
                                                    onChange={this.handleToggle}
                                                />
                                            </InputGroup>
                                            <InputGroup >
                                                <Form.Check
                                                    className={"SwitchButtonGenerator"}
                                                    type="switch"
                                                    label="Koristi Brojeve?"
                                                    id="isDigit"

                                                    checked={this.state.isDigit}
                                                    onChange={this.handleToggle}
                                                />
                                            </InputGroup>
                                            <InputGroup >
                                                <Form.Check
                                                    className={"SwitchButtonGenerator"}
                                                    type="switch"
                                                    label="Specijalni znakovi?"
                                                    id="isPunct"

                                                    checked={this.state.isPunct}
                                                    onChange={this.handleToggle}
                                                />
                                            </InputGroup>
                                        </Form>
                                    ) }


                            </Form.Group>
                            <OverlayTrigger
                                key={"changeStyle"}
                                placement={"bottom"}
                                overlay={
                                    <Tooltip id={`tooltip-bottom`}>
                                        Želite sami postaviti karakteristike lozinke?
                                    </Tooltip>
                                }
                            >
                                <InputGroup>
                                    <Form.Check
                                        className={"SwitchButtonGenerator"}
                                        type="switch"
                                        id="showManual"
                                        label="Postavi sam?"

                                        checked={this.state.showManual}
                                        onChange={this.handleToggle}
                                    />
                                </InputGroup>
                            </OverlayTrigger>

                            <InputGroup>
                                <Button
                                    variant="warning"
                                    onClick={this.callAction}
                                    className="btnSendGenerator"
                                >
                                    Generiraj
                                </Button>
                            </InputGroup>


                        </Card.Body>

                        {this.showPassword ? (
                            <Card className="CardGeneratorNewPassword">
                                <Card.Header className={"CardHeaderGeneratorNewPassword"}>
                                    Tvoja nova lozinka:
                                </Card.Header>
                                <Card.Body className={"CardBodyGeneratorNewPassword"}>
                                    {this.state.generatedPass}
                                </Card.Body>
                                <Card.Footer>
                                    <div className={"TextGenerator"}>
                                        {ReactHtmlParser(htmlRegisterGenerator)}
                                    </div>
                                    <InputGroup>
                                        <Button
                                            variant="warning"
                                            href={"/registracija"}
                                            className="btnSendGenerator"
                                        >
                                            Registracija
                                        </Button>
                                    </InputGroup>

                                </Card.Footer>

                            </Card>
                        ) : null}
                    </Card>

                </div>
            </div>
        );
    }
}
