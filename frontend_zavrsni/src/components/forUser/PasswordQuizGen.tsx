import React, { Component } from "react";
import InputGroup from "react-bootstrap/InputGroup";
import {
    Alert,
    Button,
    Card,
    Form, Modal,
    OverlayTrigger,
    Tooltip,
} from "react-bootstrap";
import logo_png from "../info/styles/background.png";
import "./Styles/QuizStyles.css"
import ReactHtmlParser from "react-html-parser";
import {CountdownCircleTimer} from "react-countdown-circle-timer";
import { Redirect } from "react-router-dom";
import jwt from "jwt-decode";
import {getUsersById} from "../../store/UserProfileService";



export default class PasswordQuiz extends Component {

    state = {
        username: "",
        password: "",
        timer: 5,
        render: false,
        attempts: 3,
        guessedWord: "",
        goToHome: false,
        helpIsOn: false,
        won: false,
        miss: false,
        loss: false,
        hiddenPassword: ""
    }

    private userID = 0;
    private userInit = false;

    private renderManual= false;
    private renderAuto= false;
    private active = false;
    private manual = true;
    private countdown = false;
    private finishedCountdown = false;
    private emptyPasswordField = true;
    private isDisabled = false;


    disableManualQuiz = (dispatch:any) => {
        this.setState({ goToHome: true });
    }

    callAction = (dispatch: any) => {
        if(this.state.password.length === 0){
            this.emptyPasswordField = false;
            setTimeout(() => {
                this.emptyPasswordField = true; this.setState({render: false})
            }, 3000);
        }
        else{
            this.emptyPasswordField=true;
        }
        this.startManualQuiz();
    }

    prepareQuiz = (dispatch:any)=>{
        this.setState({loss: false});
        this.finishedCountdown = false;
        this.setState({guessedWord: ""});
        this.setState({attempts: 3});
        this.setState({timer: 5});
        setTimeout(() => {
            this.startManualQuiz()
        }, 1);
    }

    startManualQuiz = () => {
        if(this.emptyPasswordField && !this.isDisabled){
            this.manual = false;
            this.countdown = true;
            this.active = true;
            this.setState({render: false});
            if(this.state.timer>0){
                setTimeout(() => {
                    this.setState({timer: this.state.timer - 1});
                    this.startManualQuiz();
                }, 1000);
            }
            else{
                this.finishedCountdown = true;
                this.countdown = false;
                this.setState({hiddenPassword: this.state.password})
            }
        }
        else{
            this.setState({render: false})
            this.isDisabled = false;
        }
    }

    makeGuessingAttempt = () => {
        if(this.state.guessedWord === this.state.hiddenPassword){
            this.setState({won: true})
        }
        else{
            if(this.state.attempts>1){
                this.setState({attempts: this.state.attempts-1})
                this.setState({miss: true})
            }
            else{
                this.setState({attempts: this.state.attempts-1})
                this.setState({loss: true})
            }

        }
    }

    showHelp = (dispatch: any) =>{
        this.setState({helpIsOn: !this.state.helpIsOn})
    }

    showAlert = (dispatch: any) =>{
        this.setState({miss: !this.state.miss})
    }

    onChange = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    private getUserByID = (
        userID: any,
    ) => {
        getUsersById(userID)
            .then(res => {
                if (res.status === 200) {
                    this.setState({password: res.data.generatedPass});
                    this.userInit = true;
                    console.log(this.state.password)
                }
            })
            .catch(error => {
                console.log(error);
            });
    }
    ;

    render() {
        if (this.state.goToHome) {
            return <Redirect to={"/pocetnaKorisnik/kviz"} />;
        }
        const item = sessionStorage.getItem("userInfo");
        if (item) {
            const token = jwt(item!);
            if (token) {
                // @ts-ignore
                const role = token.role;
                const currentID = token.id;
                this.userID = currentID;
                if(!this.userInit){
                    this.getUserByID(this.userID);
                }
                if (role !== "CITIZEN") {
                    return <Redirect to="/" />;
                }
            }
        } else {
            return <Redirect to="/" />;
        }

        const htmlTextQuiz =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:2.5vh;color: white;font-weight: bold">Ovaj kviz će vam pomoći u pamćenju vlastitih lozinki<br/>\n' +
            "\n" +
            "        </div>\n" +
            "    </div>";

        const htmlTextQuizCountdown =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:2.5vh;color: white;font-weight: bold">Nakon odbrojavanja počinje kviz! <br/>\n' +
            "\n" +
            "        </div>\n" +
            "    </div>";

        const htmlTextQuizFinal ="<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:2.5vh;color: white;font-weight: bold">Imate 3 pokušaja da pogodite vašu lozinku! <br/>\n' +
            "\n" +
            "        </div>\n" +
            "    </div>";

        const manualCanvas = (
            <div >
                <div className={"TextQuiz"}>
                    {ReactHtmlParser(htmlTextQuiz)}
                </div>
                <Form.Group controlId="formTitle" className="passwordInput">
                    <InputGroup className="inputGroupQuiz">
                        <OverlayTrigger
                            key={"good"}
                            placement={"bottom"}
                            overlay={
                                <Tooltip id={`tooltip-bottom`}>
                                    Ovdje je prikazana lozinka<br/>
                                    koju ste dobili generatorom!
                                </Tooltip>
                            }
                        >
                            <Form.Control
                                className={"inputFieldTimer"}
                                autoComplete="off"
                                type="text"
                                disabled
                                aria-describedby="inputGroupPrepend"
                                maxLength={30}
                                name="password"
                                value={this.state.password}
                                onChange={this.onChange}
                            />
                        </OverlayTrigger>
                    </InputGroup>

                    <InputGroup className="inputGroupQuiz">
                        <Button
                            onClick = {this.callAction}
                            className="quizButtons"
                        >
                            Krenimo!
                        </Button>

                        <Button
                            onClick={this.showHelp}
                            className="quizButtons"
                        >
                            Upute
                        </Button>
                        <Modal
                            show={this.state.helpIsOn}
                            size="lg"
                            aria-labelledby="contained-modal-title-vcenter"
                            centered
                        >
                            <Modal.Header>
                                <Modal.Title id="contained-modal-title-vcenter">
                                    Upute za kviz!
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <h4>1. Pregled lozinke</h4>
                                <p>
                                    U prvom koraku možete vidjeti lozinku koju ste<br/>
                                    generirali.
                                </p>

                                <h4>2. Odbrojavanje</h4>
                                <p>
                                    U drugom koraku imate 5 sekundi kako bi zapamtili lozinku. <br/>
                                </p>

                                <h4>3. Pogađanje</h4>
                                <p>
                                    Lozinku morate pogoditi u najviše 3 pokušaja, <br/>
                                    a jednom kada iskoristite sva 3 pokušaja imate pravo<br/>
                                    ponovno pogađati istu lozinku.
                                </p>

                            </Modal.Body>
                            <Modal.Footer>
                                <Button onClick={this.showHelp}>Close</Button>
                            </Modal.Footer>
                        </Modal>
                    </InputGroup>
                    <InputGroup className="inputGroupQuiz">
                        <Button
                            variant="danger"
                            href = {"/pocetnaKorisnik/kviz"}
                            className="quizQuitButton "
                        >
                            Odustani
                        </Button>
                    </InputGroup>
                </Form.Group>


            </div>
        )


        const countdownWindow = (
            <div>
                <div className={"TextQuiz"}>
                    {ReactHtmlParser(htmlTextQuizCountdown)}
                </div>
                <Form.Group controlId="formTitle" className="passwordInput">
                    <InputGroup className={"inputGroupQuiz"}>
                        <CountdownCircleTimer
                            size = {80}
                            isPlaying
                            duration={5}
                            colors={[["#004777", 0.33], [ "#F7B801", 0.33], ["#A30000",0]]}
                        >
                            {({ remainingTime }) => this.state.timer}
                        </CountdownCircleTimer>
                    </InputGroup>

                    <InputGroup className={"inputGroupQuiz"}>
                        <Form.Control
                            className={"inputFieldTimer"}
                            autoComplete="off"
                            type="text"
                            disabled
                            placeholder="Lozinka koju želiš zapamtiti!"
                            aria-describedby="inputGroupPrepend"
                            maxLength={30}
                            name="password"
                            value={this.state.password}
                            onChange={this.onChange}
                        />
                    </InputGroup>
                    <InputGroup className="inputGroupQuiz">
                        <Button
                            variant="danger"
                            href = {"/pocetnaKorisnik/kvizGen"}
                            className="quizQuitButton "
                        >
                            Odustani
                        </Button>
                    </InputGroup>

                </Form.Group>
            </div>


        )

        const winAlert = (
            <Modal
                show={this.state.won}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Uspješno ste pogodili lozinku!
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>{this.state.password}</h4>

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.disableManualQuiz}>Završi</Button>
                </Modal.Footer>
            </Modal>
        )

        const missAlert = (
            <Modal
                show={this.state.miss}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Netočna lozinka!
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Broj preostalih pokušaja: {this.state.attempts}</h4>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.showAlert}>Dalje</Button>
                </Modal.Footer>
            </Modal>
        )
        const lossAlert = (
            <Modal
                show={this.state.loss}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Netočna lozinka!
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Nažalost iskoristili ste sve dozvoljene pokušaje.</h4>
                    <h5>Točna lozinka: {this.state.password}</h5>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.prepareQuiz}>Ponovi</Button>
                    <Button onClick={this.disableManualQuiz}>Nova lozinka</Button>
                </Modal.Footer>
            </Modal>
        )

        const quizCanvas = (
            <div>
                <div className={"TextQuiz"}>
                    {ReactHtmlParser(htmlTextQuizFinal)}
                </div>
                <Form.Group controlId="formTitle" className="passwordInput">
                    <InputGroup className={"inputGroupQuiz"}>
                        <OverlayTrigger
                            key={"good"}
                            placement={"bottom"}
                            overlay={
                                <Tooltip id={`tooltip-bottom`}>
                                    Ovdje u potpunosti skrivena<br/>
                                    lozinka koju pogađaš!
                                </Tooltip>
                            }
                        >
                            <Form.Control
                                className={"inputField"}
                                autoComplete="off"
                                type="password"
                                disabled
                                aria-describedby="inputGroupPrepend"
                                maxLength={30}
                                name="password"
                                value={this.state.hiddenPassword}
                                onChange={this.onChange}
                            />
                        </OverlayTrigger>
                    </InputGroup>

                    <InputGroup className={"inputGroupQuiz"}>
                        <OverlayTrigger
                            key={"good"}
                            placement={"bottom"}
                            overlay={
                                <Tooltip id={`tooltip-bottom`}>
                                    Ovdje je u potpunosti upiši<br/>
                                    lozinku koju trebaš pogoditi!
                                </Tooltip>
                            }
                        >
                            <Form.Control
                                className={"inputField"}
                                autoComplete="off"
                                type="text"
                                placeholder="Upiši lozinku!"
                                aria-describedby="inputGroupPrepend"
                                maxLength={30}
                                name="guessedWord"
                                value={this.state.guessedWord}
                                onChange={this.onChange}
                            />
                        </OverlayTrigger>
                    </InputGroup>
                </Form.Group>
                <div>
                    {winAlert}
                    {missAlert}
                    {lossAlert}
                    <InputGroup className={"inputGroupQuiz"}>
                        <Button
                            onClick = {this.makeGuessingAttempt}
                            className="quizButtons"
                        >
                            Pošalji!
                        </Button>
                        <Button
                            className="quizAttempt"
                        >
                            Broj pokušaja: {this.state.attempts}
                        </Button>
                    </InputGroup>

                    <InputGroup className="inputGroupQuiz">
                        <Button
                            variant="danger"
                            href = {"/pocetnaKorisnik/kviz"}
                            className="quizQuitButton "
                        >
                            Odustani
                        </Button>
                    </InputGroup>
                </div>
            </div>
        )


        const proper = { "overflowY": "hidden" ,backgroundImage: `url(${logo_png})` } as React.CSSProperties;
        return (
            <div className="AppQuiz" style={ proper}>
                <div className="AppQuiz-content">
                    <Card className="CardQuiz" bg="success">
                        <Card.Header className={"CardTitle"}>Zapamti lozinku</Card.Header>
                        <Card.Body className={"CardBodyQuiz"}>
                            {this.manual ?
                                manualCanvas : null
                            }
                            {this.countdown ?
                                countdownWindow : null
                            }
                            {this.finishedCountdown ?
                                quizCanvas : null
                            }
                        </Card.Body>
                    </Card>
                </div>
            </div>

        );
    }
}
