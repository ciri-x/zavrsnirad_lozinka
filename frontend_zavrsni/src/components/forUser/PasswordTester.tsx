import React, { Component } from "react";
import InputGroup from "react-bootstrap/InputGroup";
import 'semantic-ui-css/semantic.min.css'
import {
    Button,
    Card,
    Form,
    OverlayTrigger,
    Tooltip,
    ListGroup, ProgressBar
} from "react-bootstrap";
import ReactHtmlParser from "react-html-parser";
import logo_png from "../info/styles/background.png";
import "./Styles/TesterStyles.css"
import {getPasswordStrength} from "../../store/PasswordTesterService";
import {Icon} from "semantic-ui-react";



export default class PasswordTester extends Component {

    state = {
        password: "",
        isLower: false,
        isUpper: false,
        isDigit: false,
        isPunct: false,
        isLonger: false,
        showParameter: false,
        progress: 0,
        barVariant: "danger",
        render:false
    }

    private testPassword = (
        password: any
    ) => {
        getPasswordStrength(password)
            .then(res => {
                if (res.status === 200) {
                    this.updateTest(res.data);
                }
            })
            .catch(error => {
                console.log(error);
            });
    };

    private updateTest = (
        data: any
    ) => {
        this.setState({progress: data[0]});
        this.formattingParameters(data[1]);
        console.log(this.state.isLower)
    }


    private formattingParameters = (
        parameters: any[]
    ) => {
        Object.values(parameters).map((val,k)=>
            {switch(k){
                case 0:
                    this.setState({isUpper:val});
                    break;
                case 1:
                    this.setState({isDigit:val});
                    break;
                case 2:
                    this.setState({isPunct:val});
                    break;
                case 3:
                    this.setState({isLower:val});
                    break;
                case 4:
                    this.setState({isLonger:val});
                    break;
                }
            }
        )
    }

    onChange = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
        this.testPassword(e.target.value);
    }

    handleToggle = (e: any) => this.setState({
        [e.target.id]: e.target.checked
    });

    render() {
        const proper = { "overflowY": "hidden" ,backgroundImage: `url(${logo_png})` } as React.CSSProperties;

        const htmlTextTester =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:2.5vh;color: white;font-weight: bold">Tester lozinke će testirati jačinu Vaše lozinke koju unesete u polje. </p>' +
            "\n" +
            '            <p style="text-align:center;font-size:2.5vh;color: white;font-weight: bold"> Jačina će biti prikazana grafički, a odabirom opcije pogledaj karakteristike možete vidjeti detaljniju analizu.</p>\n' +
            "        </div>\n" +
            "    </div>";
        return (

            <div className="AppTester" style={ proper}>
                <div className="AppTester-content">
                    <Card className="CardTester" bg="success">
                        <Card.Header className={"CardTitle"}>Tester lozinke</Card.Header>
                        {ReactHtmlParser(htmlTextTester)}
                        <Card.Body className={"CardBodyTester"} >
                            <Form.Group controlId="formPassword"  className="passwordInput" >
                                <InputGroup >
                                    <OverlayTrigger
                                        key={"testedPassword"}
                                        placement={"bottom"}
                                        overlay={
                                            <Tooltip id={`tooltip-bottom`}>
                                                Počnite pisati lozinku<br/>
                                                i saznajte njezinu jačinu.
                                            </Tooltip>
                                        }
                                    >
                                        <Form.Control
                                            className={"inputField"}
                                            type="text"
                                            autoComplete="off"
                                            placeholder="Upišite lozinku"
                                            aria-describedby="inputGroupPrepend"
                                            maxLength={20}
                                            width={30}
                                            name="password"
                                            value={this.state.password}
                                            onChange={this.onChange}
                                        />
                                    </OverlayTrigger>
                                </InputGroup>
                            </Form.Group>
                            {this.state.progress===100?
                                <ProgressBar
                                    className="progressBar"
                                    striped
                                    variant = "success"
                                    animated
                                    now={this.state.progress} />
                                : null
                            }
                            {this.state.progress===75?
                                <ProgressBar
                                    className="progressBar"
                                    striped
                                    variant = "info"
                                    animated
                                    now={this.state.progress} />
                                : null
                            }
                            {this.state.progress===50?
                                <ProgressBar
                                    className="progressBar"
                                    striped
                                    variant = "warning"
                                    animated
                                    now={this.state.progress} />
                                : null
                            }
                            {(this.state.progress===25||this.state.progress===0)?
                                <ProgressBar
                                    className="progressBar"
                                    striped
                                    variant = "danger"
                                    animated
                                    now={this.state.progress} />
                                : null
                            }
                            {!this.state.showParameter ?
                                null
                            :
                                <ListGroup as="ul" className={"listGroup"}>
                                    {!this.state.isLonger ?
                                        <ListGroup.Item variant={"danger"}>Više od 8 znakova <Icon name="close" /></ListGroup.Item>
                                        :
                                        (<ListGroup.Item variant={"success"}>Više od 8 znakova <Icon name="check" /></ListGroup.Item>)
                                    }
                                    {!this.state.isPunct ?
                                        <ListGroup.Item variant={"danger"}>Specijalni znak <Icon name="close" /></ListGroup.Item>
                                        :
                                        (<ListGroup.Item variant={"success"}>Specijalni znak <Icon name="check" /></ListGroup.Item>)
                                    }
                                    {!this.state.isDigit ?
                                        <ListGroup.Item variant={"danger"}>Broj <Icon name="close" /></ListGroup.Item>
                                        :
                                        (<ListGroup.Item variant={"success"}>Broj <Icon name="check" /></ListGroup.Item>)
                                    }
                                    {!(this.state.isUpper && this.state.isLower) ?
                                        <ListGroup.Item variant={"danger"}>Velika i mala slova <Icon name="close" /></ListGroup.Item>
                                        :
                                        (<ListGroup.Item variant={"success"}>Velika i mala slova <Icon name="check" /></ListGroup.Item>)
                                    }
                                </ListGroup>
                            }


                            <OverlayTrigger
                                key={"changeStyle"}
                                placement={"bottom"}
                                overlay={
                                    <Tooltip id={`tooltip-bottom`}>
                                        Želite pogledati karakteristike lozinke?
                                    </Tooltip>
                                }
                            >
                                <InputGroup className={"newButton"}>
                                    <Form.Check
                                        type="switch"
                                        id="showParameter"
                                        label="Pogledaj karakteristike?"

                                        checked={this.state.showParameter}
                                        onChange={this.handleToggle}
                                    />
                                </InputGroup>
                            </OverlayTrigger>
                        </Card.Body>
                    </Card>
                </div>
            </div>
        );
    }
}
