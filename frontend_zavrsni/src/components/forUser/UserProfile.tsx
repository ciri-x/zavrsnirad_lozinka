import React, { Component } from "react";
import InputGroup from "react-bootstrap/InputGroup";
import {
    Alert,
    Button, Card, ListGroup, OverlayTrigger, Popover,
} from "react-bootstrap";
import logo_png from "../info/styles/background.png";
import "./Styles/UserProfileStyle.css"
import Form from "react-bootstrap/Form";
import {changePassProfile, changeUserName, getUsersById} from "../../store/UserProfileService";
import {Redirect } from "react-router-dom";
import jwt from "jwt-decode";
import {Icon} from "semantic-ui-react";
import {getPasswordStrength} from "../../store/PasswordTesterService";
import ReactHtmlParser from "react-html-parser";


export default class UserProfile extends Component {

    state = {
        isLower: false,
        isUpper: false,
        isDigit: false,
        isPunct: false,
        progress: 0,
        password:"",
        render:false,
        username: "",
        email:"",
        isLonger: false
    }

    private userID = 0;
    private userInit = false;
    public passwordSlab = true;
    private usernameExists=false;
    private usernameChanged=false;
    private passwordChanged=false;

    private testPassword = (
        password: any
    ) => {
        getPasswordStrength(password)
            .then(res => {
                if (res.status === 200) {
                    this.updateTest(res.data);
                }
            })
            .catch(error => {
                console.log(error);
            });
    };

    private updateTest = (
        data: any
    ) => {
        this.setState({progress: data[0]});
        this.formattingParameters(data[1]);
    }

    private formattingParameters = (
        parameters: any[]
    ) => {
        Object.values(parameters).map((val,k)=>
            {switch(k){
                case 0:
                    this.setState({isUpper:val});
                    break;
                case 1:
                    this.setState({isDigit:val});
                    break;
                case 2:
                    this.setState({isPunct:val});
                    break;
                case 3:
                    this.setState({isLower:val});
                    break;
                case 4:
                    this.setState({isLonger:val});
                    break;
            }
            }
        )
    }

    private getUserByID = (
        userID: any,
    ) => {
        getUsersById(userID)
            .then(res => {
                if (res.status === 200) {
                    this.setState({username: res.data.username, email:res.data.email});
                    this.userInit = true;
                    console.log(this.state.username)
                }
            })
            .catch(error => {
                console.log(error);
            });
    }
    ;

    private callActionPassword = (dispatch: any) => {

        if (this.state.progress < 100) {
            this.passwordSlab = false;
            setTimeout(() => {
                this.passwordSlab = true; this.setState({render: false})
            }, 3000);
            this.callingServicePassword(this.state.password, this.state.email);
        } else {
            this.passwordSlab = true;
            this.callingServicePassword(this.state.password, this.state.email);
        }

    };

    private callingServicePassword = (password: any, mail:any) => {

        if(this.passwordSlab){
            changePassProfile(mail, password)
                .then(res => {
                    if (res === true) {
                        this.passwordChanged = true;
                        this.setState({password: ""})
                        this.setState({ ...this.state });
                    } else {
                        this.passwordChanged = false;
                        this.setState({ ...this.state });
                    }
                })
                .catch(error => {
                    this.usernameExists = true;
                    this.setState({ ...this.state });
                    console.log(error);
                });
        }
        else{
            this.setState({ ...this.state });
        }

    };


    private callActionUsername = (dispatch: any) => {
            this.callingServiceUsername(this.state.username, this.state.email);
    };

    private callingServiceUsername = (user: any, mail:any) => {
         changeUserName(mail, user)
                .then(res => {
                    if (res === true) {
                        this.usernameChanged = true;
                        this.usernameExists = false;
                        this.setState({ ...this.state });
                    } else {
                        this.usernameExists = true;
                        this.usernameChanged = false;
                        this.setState({ ...this.state });
                    }
                })
                .catch(error => {
                    this.usernameExists = true;
                    this.setState({ ...this.state });
                    console.log(error);
                });
    };


    stateChange = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    onChange = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
        this.testPassword(e.target.value);
    }


    render() {

        const item = sessionStorage.getItem("userInfo");
        if (item) {
            const token = jwt(item!);
            if (token) {
                // @ts-ignore
                const role = token.role;
                const currentID = token.id;
                this.userID = currentID;
                if(!this.userInit){
                    this.getUserByID(this.userID);
                }
                if (role !== "CITIZEN") {
                    return <Redirect to="/" />;
                }
            }
        } else {
            return <Redirect to="/" />;
        }

        const htmlTextProfile =
            "<!-- Add a background color and large text to the whole page -->\n" +
            '<div class="w3-sand w3-grayscale w3-large">\n' +
            "\n" +
            "    <!-- About Container -->\n" +
            '    <div class="w3-container" id="about">\n' +
            '        <div class="w3-content" style="max-width:700px text-align:center font-family: Impact">\n' +
            "\n" +
            '            <p style="text-align:center;font-size:2.5vh;color: white;font-weight: bold">Ovdje možete promijeniti vaše trenutne korisničke podatke. </p>' +
            "\n" +
            '            <p style="text-align:center;font-size:2.5vh;color: white;font-weight: bold"> Nakon upisa novog podatka pritisnite Spremi.</p>\n' +
            "        </div>\n" +
            "    </div>";

        const proper = { "overflowY": "hidden" ,backgroundImage: `url(${logo_png})` } as React.CSSProperties;
        return (
            <div className="AppUserProfile" style={ proper}>
                <div className="AppUserProfile-content">
                    <Card className="CardGenerator" bg="success">
                        <Card.Header className={"CardTitleAppSignIn"}>Profil</Card.Header>
                        {ReactHtmlParser(htmlTextProfile)}
                        <Card.Body className={"CardBodyAppSignIn"}>
                            <Form.Group controlId="formUsername" className="passwordInputAppUserProfile">
                                <Form.Label>Nadimak</Form.Label>
                                <InputGroup className="Title">
                                    <Form.Control
                                        type="username"
                                        autoComplete="off"
                                        placeholder="Unesite nadimak"
                                        name={"username"}
                                        onChange={this.stateChange}
                                        value={this.state.username}
                                    />
                                </InputGroup>
                                <InputGroup className="inputGroupAppSignIn">

                                    <Button
                                        className={"AppSignInButtons"}
                                        onClick = {this.callActionUsername}
                                    >
                                        Spremi
                                    </Button>

                                </InputGroup>
                                <InputGroup className="alertInput">
                                    {this.usernameExists ? (
                                        <Alert variant={"warning"}>Ovaj nadimak već postoji</Alert>
                                    ) : null}
                                    {this.usernameChanged ? (
                                        <Alert variant={"success"}>Nadimak uspješno promijenjen</Alert>
                                    ) : null}

                                </InputGroup>
                            </Form.Group>
                            <Form.Group controlId="formBasicPassword"  className="passwordInputAppSignIn">
                                <Form.Label>Lozinka</Form.Label>
                                <InputGroup className="Title">
                                    <OverlayTrigger
                                        key={"testedPassword"}
                                        placement={"right-start"}
                                        overlay={
                                            <Popover id={`popover-basic`} className = {"tooltipAppSignIn"}>
                                                <Popover.Title as="h3">Lozinka treba sadržavati:</Popover.Title>
                                                <Popover.Content >
                                                    <ListGroup as="ul" className={"popoverContent"}>
                                                        {!this.state.isLonger ?
                                                            <ListGroup.Item variant={"danger"}>Više od 8 znakova <Icon name="close" /></ListGroup.Item>
                                                            :
                                                            (<ListGroup.Item variant={"success"}>Više od 8 znakova <Icon name="check" /></ListGroup.Item>)
                                                        }
                                                        {!this.state.isPunct ?
                                                            <ListGroup.Item variant={"danger"}>Specijalni znak <Icon name="close" /></ListGroup.Item>
                                                            :
                                                            (<ListGroup.Item variant={"success"}>Specijalni znak <Icon name="check" /></ListGroup.Item>)
                                                        }
                                                        {!this.state.isDigit ?
                                                            <ListGroup.Item variant={"danger"}>Broj <Icon name="close" /></ListGroup.Item>
                                                            :
                                                            (<ListGroup.Item variant={"success"}>Broj <Icon name="check" /></ListGroup.Item>)
                                                        }
                                                        {!(this.state.isUpper && this.state.isLower) ?
                                                            <ListGroup.Item variant={"danger"}>Velika i mala slova <Icon name="close" /></ListGroup.Item>
                                                            :
                                                            (<ListGroup.Item variant={"success"}>Veliko i mala slova <Icon name="check" /></ListGroup.Item>)
                                                        }
                                                    </ListGroup>
                                                </Popover.Content>
                                            </Popover>
                                        }
                                    >
                                        <Form.Control
                                            type="password"
                                            autoComplete="off"
                                            placeholder="Lozinka"
                                            name={"password"}
                                            onChange={this.onChange}
                                            value={this.state.password}
                                        />
                                    </OverlayTrigger>
                                </InputGroup>
                                <InputGroup className="alertInput">
                                    {!this.passwordSlab ? (
                                        <Alert variant={"danger"}>Loša lozinka!</Alert>
                                    ) : null}
                                </InputGroup>
                                <InputGroup className="inputGroupAppSignIn">

                                    <Button
                                        className={"AppSignInButtons"}
                                        onClick={this.callActionPassword}
                                    >
                                        Spremi
                                    </Button>
                                </InputGroup>
                                <InputGroup className="alertInput">
                                    {this.passwordChanged ? (
                                        <Alert variant={"success"}>Lozinka uspješno promijenjena</Alert>
                                    ) : null}

                                </InputGroup>
                            </Form.Group>
                            <InputGroup className="inputGroupAppSignIn">
                                <Button
                                    className={"AppSignInQuitButton"}
                                    href={"/pocetnaKorisnik"}
                                >
                                    Odustani
                                </Button>

                            </InputGroup>

                        </Card.Body>
                    </Card>
                </div>
            </div>
        )}

}
