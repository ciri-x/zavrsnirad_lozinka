import Axios from "axios";
import {POCETNA_ADRESA} from "./BaseUrl";


export const getPasswordStrength = (password: any) =>
    Axios.get(POCETNA_ADRESA + "/testPassword?" + "password="+password )
        .then(function(res) {
            if (res.status === 200) {
                return res;
            }
        })
        .catch(function(error) {
            console.log(error);
            return error;
        });
