import Axios from "axios";
import {POCETNA_ADRESA} from "./BaseUrl";



export const getGeneratedPasswordManual = (word: any, isUpper: any, isDigit: any, isPunct: any) =>
    Axios.get(POCETNA_ADRESA + "/passwordGen?" + "isCapitalLetters="+isUpper+"&isDigit="+isDigit+"&isPunctuation="+isPunct+"&word=" + word )
        .then(function(res) {
            if (res.status === 200) {
                return res;
            }
        })
        .catch(function(error) {
            console.log(error);
            return error;
        });

export const getGeneratedPasswordAuto = (word: any, strength: any) =>
    Axios.get(POCETNA_ADRESA + "/passwordGen/Auto?" + "strength="+strength+"&word=" + word )
        .then(function(res) {
            if (res.status === 200) {
                return res;
            }
        })
        .catch(function(error) {
            console.log(error);
            return error;
        });

export const putGeneratedPassword = (password: any, username: any) =>
    Axios.put(POCETNA_ADRESA + "/passwordGen/Quiz?generatedPass="+password+"&username=" + username)
        .then(function(res) {
            if (res.status === 200) {
                console.log("pass je promjenjen");
                return true;
            } else {
                console.log("korisnik nije potvrden");
                return false;
            }
        })

export const getGeneratedPassword = (username: any) =>
    Axios.get(POCETNA_ADRESA + "/passwordGen/Quiz?username="+username)
        .then(function(res) {
            if (res.status === 200) {
                return res;
            }
        })
        .catch(function(error) {
            console.log(error);
            return error;
        });