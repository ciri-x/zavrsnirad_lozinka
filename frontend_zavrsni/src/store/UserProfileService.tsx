import Axios from "axios";
import {config} from "./token";
import {POCETNA_ADRESA} from "./BaseUrl";

export const getUsersById = (userID : any) =>
    Axios.get(POCETNA_ADRESA + "/users/" + userID,config)
        .then(function(res) {
            if (res.status === 200) {
                return res;
            }
        })
        .catch(function(error) {
            console.log(error);
            return error;
        });

export const changeUserName= (email: any, newUsername: any) =>
    Axios.put(POCETNA_ADRESA + "/registration/changeUsername?newUsername="+newUsername+"&email=" + email)
        .then(function(res) {
            if (res.status === 200) {
                console.log("user je promjenjen");
                return true;
            } else {
                console.log("korisnik nije potvrden");
                return false;
            }
        })

export const changePassProfile= (email: any, newPassword: any) =>
    Axios.put(POCETNA_ADRESA + "/registration/changePasswordProfile?newPassword="+newPassword+"&email=" + email)
        .then(function(res) {
            if (res.status === 200) {
                console.log("password je promjenjen");
                return true;
            } else {
                console.log("korisnik nije potvrden");
                return false;
            }
        })