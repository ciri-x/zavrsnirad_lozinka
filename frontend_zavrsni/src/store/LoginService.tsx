import Axios from "axios";
import {POCETNA_ADRESA} from "./BaseUrl";


export const signin = (credentials: any) =>
    Axios.post(POCETNA_ADRESA + "/login", credentials)
        .then(function(res) {
            if (res.status === 200) {
                sessionStorage.setItem("userInfo", JSON.stringify(res.data));
                console.log("response exists");
                return res;
            }
        })
        .catch(function(error) {
            console.log(error);
            return error;
        });


export const register = (newUser: any) =>
    Axios.post(POCETNA_ADRESA + "/registration", newUser)
        .then(function(res) {
            if (res.status === 200) {
                console.log("user je registriran");
                return true;
            } else {
                console.log("korisnik je već registriran");
                return false;
            }
        })
        .catch(function(error) {
            console.log(error);
            return error;
        });

export const confirm = (token: any) =>
    Axios.put(POCETNA_ADRESA + "/registration/confirm?token=" + token)
        .then(function(res) {
            if (res.status === 200) {
                console.log("user je potvrden");
                return true;
            } else {
                console.log("korisnik nije potvrden");
                return false;
            }
        })

export const resetPass = (mail: any) =>
    Axios.post(POCETNA_ADRESA + "/registration/changePassword?email=" + mail)
        .then(function(res) {
            if (res.status === 200) {
                console.log("mail je poslan");
                return true;
            } else {
                console.log("korisnik nije potvrden");
                return false;
            }
        })

export const confirmChange= (token: any, newPass: any) =>
    Axios.put(POCETNA_ADRESA + "/registration/confirmChange?newPassword="+newPass+"&token=" + token)
        .then(function(res) {
            if (res.status === 200) {
                console.log("pass je promjenjen");
                return true;
            } else {
                console.log("korisnik nije potvrden");
                return false;
            }
        })

export const resetUsername = (mail: any) =>
    Axios.post(POCETNA_ADRESA + "/registration/changeUsername?email=" + mail)
        .then(function(res) {
            if (res.status === 200) {
                console.log("mail je poslan");
                return true;
            } else {
                console.log("korisnik nije potvrden");
                return false;
            }
        })

export const confirmUsernameChange= (token: any, newUsername: any) =>
    Axios.put(POCETNA_ADRESA + "/registration/confirmChangeUserName?newUsername="+newUsername+"&token=" + token)
        .then(function(res) {
            if (res.status === 200) {
                console.log("username je promjenjen");
                return true;
            } else {
                console.log("korisnik nije potvrden");
                return false;
            }
        })