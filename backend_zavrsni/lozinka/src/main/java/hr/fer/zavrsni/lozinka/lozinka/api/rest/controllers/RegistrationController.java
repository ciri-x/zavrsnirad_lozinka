package hr.fer.zavrsni.lozinka.lozinka.api.rest.controllers;

import hr.fer.zavrsni.lozinka.lozinka.api.model.User;
import hr.fer.zavrsni.lozinka.lozinka.api.rest.dtos.CreateUserDTO;
import hr.fer.zavrsni.lozinka.lozinka.api.rest.services.EmailSenderService;
import hr.fer.zavrsni.lozinka.lozinka.api.rest.services.UsersService;
import hr.fer.zavrsni.lozinka.lozinka.exception.ForbiddenException;
import hr.fer.zavrsni.lozinka.lozinka.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private EmailSenderService emailSenderService;

    @PostMapping("")
    public User register(@RequestBody CreateUserDTO user) {

        User newUser = usersService.saveUser(user.getUsername(), user.getPassword(), user.getFirstName(), user.getLastName(),
                user.getEmail(), user.getRole(), false, UUID.randomUUID().toString());

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(newUser.getEmail());
        mailMessage.setSubject("Aktiviranje računa!");
        mailMessage.setFrom("lozinka.zavrsni@gmail.com");
        mailMessage.setText("Za potvrdu računa otvorite link i upišite aktivacijski kod : "
                +"https://lozinka.herokuapp.com/potvrdaRegistracije "
                +"Aktivacijski kod: " + newUser.getConfirmationToken());

        emailSenderService.sendEmail(mailMessage);
        return newUser;
    }

    @PutMapping("/confirm")
    public void confirmRegistration(@RequestParam String token){
        User confirmedUser = usersService.findByConfirmationToken(token);

        if(confirmedUser == null){
            throw new ForbiddenException("No user found");
        }
        else{
            usersService.updateUser(confirmedUser);
        }
    }

    @PostMapping("/changePassword")
    public User changePasswordRequest(@RequestParam String email) {
        User confirmedUser = usersService.getUserByEmail(email).orElseThrow(() -> new NotFoundException("User not found"));

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject("Promjena lozinke!");
        mailMessage.setFrom("lozinka.zavrsni@gmail.com");
        mailMessage.setText("Za promjenu lozinke otvorite navedeni link : "
                //+"http://localhost:3000/promjenaLozinke"
                +"https://lozinka.herokuapp.com/promjenaLozinke"
                +" Kod za promjenu lozinke: " + confirmedUser.getConfirmationToken());

        emailSenderService.sendEmail(mailMessage);
        return confirmedUser;
    }

    @PutMapping("/confirmChange")
    public void confirmChange(@RequestParam String newPassword,@RequestParam String token){
        User confirmedUser = usersService.findByConfirmationToken(token);

        if(confirmedUser == null){
            throw new ForbiddenException("No user found");
        }
        else{
            usersService.updateUserPassword(confirmedUser, newPassword);
        }
    }

    @PostMapping("/changeUsername")
    public User changeUsernameRequest(@RequestParam String email) {
        User confirmedUser = usersService.getUserByEmail(email).orElseThrow(() -> new NotFoundException("User not found"));

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject("Promjena nadimka!");
        mailMessage.setFrom("lozinka.zavrsni@gmail.com");
        mailMessage.setText("Za promjenu nadimka otvorite navedeni link : "
                //+"http://localhost:3000/promjenaLozinke"
                +"https://lozinka.herokuapp.com/promjenaNadimka"
                +" Kod za promjenu nadimka: " + confirmedUser.getConfirmationToken());

        emailSenderService.sendEmail(mailMessage);
        return confirmedUser;
    }
    @PutMapping("/confirmChangeUserName")
    public void confirmUserChange(@RequestParam String newUsername,@RequestParam String token){
        User confirmedUser = usersService.findByConfirmationToken(token);

        if(confirmedUser == null){
            throw new ForbiddenException("No user found");
        }
        else{
            usersService.updateUsername(confirmedUser, newUsername);
        }
    }

    @PutMapping("/changeUsernameProfile")
    public void changeUserName(@RequestParam String newUsername, @RequestParam String email){
        User confirmedUser = usersService.getUserByEmail(email).orElseThrow(() -> new NotFoundException("User not found"));

        if(confirmedUser == null){
            throw new ForbiddenException("No user found");
        }
        else{
            usersService.updateUsername(confirmedUser, newUsername);
        }
    }

    @PutMapping("/changePasswordProfile")
    public void changePassword(@RequestParam String newPassword, @RequestParam String email){
        User confirmedUser = usersService.getUserByEmail(email).orElseThrow(() -> new NotFoundException("User not found"));

        if(confirmedUser == null){
            throw new ForbiddenException("No user found");
        }
        else{
            usersService.updateUserPassword(confirmedUser, newPassword);
        }
    }


}