package hr.fer.zavrsni.lozinka.lozinka.api.rest.controllers;

import hr.fer.zavrsni.lozinka.lozinka.api.rest.services.PasswordTesterService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
@RequestMapping("/testPassword")
public class PasswordTesterController {

    @Autowired
    private PasswordTesterService passwordTester;


    @GetMapping("")
    @ResponseBody
    public ArrayList testPasswordStrength(String password) {
        return passwordTester.testPasswordStrength(password);
    }

}
