package hr.fer.zavrsni.lozinka.lozinka.api.rest.controllers;

import hr.fer.zavrsni.lozinka.lozinka.api.rest.dtos.CreateUserDTO;
import hr.fer.zavrsni.lozinka.lozinka.api.rest.services.EmailSenderService;
import hr.fer.zavrsni.lozinka.lozinka.exception.ForbiddenException;
import hr.fer.zavrsni.lozinka.lozinka.exception.NotFoundException;
import hr.fer.zavrsni.lozinka.lozinka.api.model.User;
import hr.fer.zavrsni.lozinka.lozinka.api.rest.services.UsersService;
import hr.fer.zavrsni.lozinka.lozinka.util.AuthorizationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private AuthorizationUtil authorizationUtil;

    @GetMapping("")
    //@Secured("ROLE_ADMIN")
    public List<User> getAllUsers() {
        return usersService.getUsers();
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable int id) {
            return usersService.getUser(id).orElseThrow(() -> new NotFoundException("User not found"));
    }

    @DeleteMapping("/{id}")
    //@Secured("ROLE_ADMIN")
    public void deleteUser(@PathVariable int id) {
        usersService.deleteUser(id);
    }

}
