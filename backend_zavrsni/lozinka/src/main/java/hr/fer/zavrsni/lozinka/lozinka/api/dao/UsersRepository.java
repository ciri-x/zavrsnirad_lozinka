package hr.fer.zavrsni.lozinka.lozinka.api.dao;

import hr.fer.zavrsni.lozinka.lozinka.api.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Integer> {
    Optional<User> findByUsername(String username);
    Optional<User> findByEmail(String email);
    User findByConfirmationToken(String confirmationToken);
}