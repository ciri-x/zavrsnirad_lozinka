package hr.fer.zavrsni.lozinka.lozinka.api.model.enums;

public enum Role {
    ADMIN,
    CITIZEN
}
