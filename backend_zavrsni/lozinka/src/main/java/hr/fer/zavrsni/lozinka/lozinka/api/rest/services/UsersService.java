package hr.fer.zavrsni.lozinka.lozinka.api.rest.services;

import hr.fer.zavrsni.lozinka.lozinka.api.dao.UsersRepository;
import hr.fer.zavrsni.lozinka.lozinka.api.model.User;
import hr.fer.zavrsni.lozinka.lozinka.api.model.enums.Role;
import hr.fer.zavrsni.lozinka.lozinka.api.rest.dtos.CreateUserDTO;
import hr.fer.zavrsni.lozinka.lozinka.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class UsersService {

    @Autowired
    private UsersRepository usersRepository;

    public List<User> getUsers() {
        return usersRepository.findAll();
    }

    public Optional<User> getUser(int id) {
        return usersRepository.findById(id);
    }

    public void deleteUser(int id) {
        usersRepository.deleteById(id);
    }

    public Optional<User> getUserByUsername(String username) {
        return usersRepository.findByUsername(username);
    }

    public Optional<User> getUserByEmail(String email){ return usersRepository.findByEmail(email); }

    public User findByConfirmationToken(String confirmationToken){ return usersRepository.findByConfirmationToken(confirmationToken); }

    public User saveUser(String username, String password, String firstName, String lastName
                         , String email, Role role, boolean isConfirmed, String confirmationToken) {
        Assert.hasText(username, "Username must be given");
        Assert.hasText(password, "Password must be given");
        Assert.isTrue(password.length() >= 8, "Password length must be at least 8");
        Assert.notNull(role, "Role must be given");

        if (getUserByUsername(username).isPresent()) {
            throw new BadRequestException("Username already used.");
        }
        if (email != null && getUserByEmail(email).isPresent()) {
            throw new BadRequestException("Email already used.");
        }

        User user = new User(username, password, firstName,
                lastName, email, role, isConfirmed, confirmationToken, "");
        usersRepository.save(user);
        return user;
    }
    public User updateUser(User user){
        user.setConfirmed(true);

        User updatedUser = usersRepository.save(user);

        return updatedUser;
    }

    public User updateUsername(User user, String newUserName){

        if (getUserByUsername(newUserName).isPresent()) {
            throw new BadRequestException("Username already used.");
        }
        user.setUsername(newUserName);

        User updatedUser = usersRepository.save(user);

        return updatedUser;
    }


    public User updateUserGeneratedPassword(User user, String generatedPass){
        user.setGeneratedPass(generatedPass);

        User updatedUser = usersRepository.save(user);

        return updatedUser;
    }

    public User updateUserPassword(User user, String password){
        user.setPassword(password);

        User updatedUser = usersRepository.save(user);

        return updatedUser;
    }

}