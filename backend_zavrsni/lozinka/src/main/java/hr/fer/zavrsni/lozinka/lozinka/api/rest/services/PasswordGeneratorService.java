package hr.fer.zavrsni.lozinka.lozinka.api.rest.services;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class PasswordGeneratorService  {
    private static final String DIGITS = "0123456789";
    private static final String PUNCTUATION = "!@#$%&*()_+-=[]|,./?><";
    private static final int WEAK_MAX_LENGTH = 7;
    private static final int MIN_LENGTH_PRETTY = 6;
    private static final int MIN_LENGTH = 8;
    private static final int MAX_LENGTH = 12;

    public String generatePassword(String word, boolean isCapitalLetter, boolean isDigit, boolean isPunctuation ){
        String newPassword;
        newPassword = word.toLowerCase();
        if(isCapitalLetter){
            newPassword = putRandomCharsToUpperLetters(newPassword);
        }
        if(isDigit && isPunctuation){
            newPassword = addRandomDigitAndPunctuation(newPassword);
        }
        if(isDigit&&!isPunctuation){
            newPassword = addRandomDigitOrPunctPassword(newPassword, false);
        }
        if(isPunctuation&&!isDigit){
            newPassword = addRandomDigitOrPunctPassword(newPassword, true);
        }
        return newPassword;
    }

    public String generatePasswordAuto(String word, String strength){
        String newPassword;
        newPassword = word.toLowerCase();
        boolean boolDigit = getRandomBoolean();
        boolean boolPunct = getRandomBoolean();
        boolean boolCapital = getRandomBoolean();
        int makeRandomFalse = getRandomNumberInRange(0,2);

        if (strength.equals("weak")){
            if(word.length()==7){
                boolPunct=false;
                boolDigit=false;
            }
            if(word.length()==6){
                switch(makeRandomFalse) {
                    case 0:
                       boolDigit=false;
                        break;
                    case 1:
                        boolPunct=false;
                        break;
                    case 2:
                        boolPunct=false;
                        break;
                }
            }
            if(boolCapital&&boolDigit&&boolPunct){
                switch(makeRandomFalse) {
                    case 0:
                        boolDigit=false;
                        break;
                    case 1:
                        boolPunct=false;
                        break;
                    case 2:
                        boolCapital=false;
                        break;
                }
            }

            newPassword=getWeakPassword(newPassword, boolCapital, boolDigit, boolPunct);
        }
        if(strength.equals("good")){
            boolean isLonger;
            if(word.length()>=8){
                isLonger = true;
            }
            else{
                isLonger = getRandomBoolean(); //is good pass longer than 8, if not we need all other to be true
            }

            if(isLonger){
                if(boolCapital&&boolDigit&&boolPunct){
                    boolCapital= false;
                }
                else if(!boolDigit&&!boolPunct){
                    boolDigit=true;
                }
                newPassword = generatePassword(newPassword, boolCapital,boolDigit,boolPunct);
            }
            else{
                newPassword=getWeakPassword(newPassword,true,true,true);
            }
        }
        if(strength.equals("strong")){
            newPassword=generatePassword(newPassword, true, true, true);
        }
        return newPassword;
    }

    private static String getWeakPassword(String word, boolean isCapitalLetter, boolean isDigit, boolean isPunctuation){
        if(isCapitalLetter){
            word = putRandomCharsToUpperLetters(word);
        }
        if(isDigit&&isPunctuation){
            word = addRandomDigitAndPunctuationWeakPassword(word);
        }
        if(isDigit&&!isPunctuation){
            word = addRandomDigitOrPunctWeakPassword(word, false);
        }
        if(isPunctuation&&!isDigit){
            word = addRandomDigitOrPunctWeakPassword(word, true);
        }
        return word;
    }

    private static String putRandomCharsToUpperLetters(String word){
        int wordSize = word.length();
        int numberOfUpperLetters;
        if(wordSize==1){
            numberOfUpperLetters = wordSize - getRandomNumberInRange(0, wordSize-1);
        }
        else{
            numberOfUpperLetters = wordSize - getRandomNumberInRange(1, wordSize-1);
        }

        for (int i=0; i<numberOfUpperLetters;i++){
            int index = getRandomNumberInRange(0, wordSize-1);
            char toUpper = word.charAt(index);
            toUpper = Character.toUpperCase(toUpper);
            if (index == 0){
                word = toUpper + word.substring(1);
            }
            else if(index == wordSize-1){
                word = word.substring(0,wordSize-1) + toUpper;
            }
            else{
                word = word.substring(0,index) + toUpper + word.substring(index+1);
            }
        }
        return word;
    }

    private static String addRandomDigitAndPunctuation(String word){
        int wordSize = word.length();
        int maxNumOfExtraChar =  MAX_LENGTH - wordSize;
        int numberOfDigits;
        int numberOfPunctuations;
        int minNumOfExtraChar = MIN_LENGTH - wordSize;
        if(wordSize>MIN_LENGTH_PRETTY){
            if (wordSize<MIN_LENGTH){
                numberOfDigits = getRandomNumberInRange(minNumOfExtraChar, maxNumOfExtraChar-1);
            }
            else{
                numberOfDigits = getRandomNumberInRange(1, maxNumOfExtraChar-1);
            }
            numberOfPunctuations = getRandomNumberInRange(1, maxNumOfExtraChar-numberOfDigits);
        }
        else{
            numberOfDigits = getRandomNumberInRange(1, minNumOfExtraChar-1);
            numberOfPunctuations = minNumOfExtraChar - numberOfDigits;
        }

        return addDigitsAndPunct(word,numberOfDigits, numberOfPunctuations);
    }
    private static String addRandomDigitOrPunctPassword(String word, boolean punctOrDigit){
        int wordSize = word.length();
        int numberOfChars;
        int maxNumOfChars =  MAX_LENGTH - wordSize;
        int minNumOfChars = MIN_LENGTH - wordSize;
        if(wordSize>MIN_LENGTH_PRETTY){
            if (wordSize<MIN_LENGTH){
                numberOfChars = getRandomNumberInRange(minNumOfChars, maxNumOfChars);
            }
            else{
                numberOfChars = getRandomNumberInRange(1, maxNumOfChars);
            }
        }
        else{
            numberOfChars = minNumOfChars;
        }

        if(punctOrDigit){//if true it adds punct
            return addPunctuations(word,numberOfChars);
        }
        else{
            return addDigits(word,numberOfChars);
        }

    }


    private static String addRandomDigitAndPunctuationWeakPassword(String word){
        int wordSize = word.length();
        int maxNumOfExtraChar =  WEAK_MAX_LENGTH - wordSize;
        int numberOfDigits = getRandomNumberInRange(1, maxNumOfExtraChar-1);
        int numberOfPunctuations = getRandomNumberInRange(1, maxNumOfExtraChar-numberOfDigits);
        return addDigitsAndPunct(word,numberOfDigits, numberOfPunctuations);
    }

    private static String addRandomDigitOrPunctWeakPassword(String word, boolean punctOrDigit){
        int wordSize = word.length();
        int maxNumOfChars =  WEAK_MAX_LENGTH - wordSize;
        int numberOfChars = getRandomNumberInRange(1, maxNumOfChars);
        if(punctOrDigit){//if true it adds punct
            return addPunctuations(word,numberOfChars);
        }
        else{
            return addDigits(word,numberOfChars);
        }

    }


    private static String addDigitsAndPunct(String word, int numberOfDigits, int numberOfPunctuations){

        while((numberOfDigits>0)||(numberOfPunctuations>0)){
            boolean addDigitOrPunct = getRandomBoolean();
            if (addDigitOrPunct){
                if(numberOfDigits>0){
                    numberOfDigits--;
                    word=addDigits(word,1);
                }
                else{
                    numberOfPunctuations--;
                    word=addPunctuations(word,1);
                }
            }
            else{
                if(numberOfPunctuations>0){
                    numberOfPunctuations--;
                    word=addPunctuations(word,1);
                }
                else{
                    numberOfDigits--;
                    word=addDigits(word,1);
                }
            }
        }
        return word;
    }

    private static String addDigits(String word, int numberOfDigits){
        while(numberOfDigits>0){
            boolean addToPrefixOrSufix = getRandomBoolean(); //0 - prefix, 1 sufix
            char digit = DIGITS.charAt(getRandomNumberInRange(0,DIGITS.length()-1));
            if (addToPrefixOrSufix){
                word = digit + word;
            }
            else{
                word = word + digit;
            }
            numberOfDigits--;
        }
        return word;
    }
    private static String addPunctuations(String word, int numberOfPunctuations){
        while(numberOfPunctuations>0){
            boolean addToPrefixOrSufix = getRandomBoolean(); //0 - prefix, 1 sufix
            char punct = PUNCTUATION.charAt(getRandomNumberInRange(0,PUNCTUATION.length()-1));
            if (addToPrefixOrSufix){
                word = punct + word;
            }
            else{
                word = word + punct;
            }
            numberOfPunctuations--;
        }
        return word;
    }

    private static int getRandomNumberInRange(int min, int max) {

        Random r = new Random();
        return r.ints(min, (max + 1)).findFirst().getAsInt();
    }

    private static boolean getRandomBoolean(){
        return Math.random() < 0.5;
    }
}
