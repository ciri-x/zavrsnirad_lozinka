package hr.fer.zavrsni.lozinka.lozinka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LozinkaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LozinkaApplication.class, args);
	}

}
