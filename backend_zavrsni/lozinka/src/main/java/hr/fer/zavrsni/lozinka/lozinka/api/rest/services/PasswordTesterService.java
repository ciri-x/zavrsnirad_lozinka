package hr.fer.zavrsni.lozinka.lozinka.api.rest.services;

import org.springframework.stereotype.Service;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import java.util.regex.Pattern;

@Service
public class PasswordTesterService  {
    private static final int TARGET_LENGTH = 8;

    private static final Pattern[] inputRegexes = new Pattern[4];

    static {
        inputRegexes[0] = Pattern.compile(".*[A-Z].*");
        inputRegexes[1] = Pattern.compile(".*[a-z].*");
        inputRegexes[2] = Pattern.compile(".*\\d.*");
        inputRegexes[3] = Pattern.compile(".*[`~!@#$%^&*()\\-_=+\\\\|\\[{\\]};:'\",<.>/?].*");
    }

    public ArrayList testPasswordStrength(String password){
        ArrayList passwordStrength = new ArrayList();
        double progress = 0;
        HashMap<String,Boolean> booleanMap = new HashMap<>();
        boolean isLonger = false;

        if(password.length()>=TARGET_LENGTH){
            booleanMap.put("isLonger", Boolean.TRUE);
            isLonger=true;
        }
        else{
            booleanMap.put("isLonger", Boolean.FALSE);
        }

        boolean isPunct = isMatchingRegex(password, booleanMap,"isPunct");
        boolean isDigit = isMatchingRegex(password, booleanMap,"isDigit");
        boolean isUpper = isMatchingRegex(password, booleanMap,"isUpper");
        boolean isLower = isMatchingRegex(password, booleanMap,"isLower");

        progress = progressCheck(isDigit, isPunct, isLonger, isLower, isUpper);

        passwordStrength.add(progress);
        passwordStrength.add(booleanMap);
        return passwordStrength;
    }


    private static boolean isMatchingRegex(String input, Map<String, Boolean> parameterMap, String parameter) {

        boolean inputMatches = false;
        switch (parameter){
            case "isUpper":
                inputMatches = inputRegexes[0].matcher(input).matches();
                break;
            case "isLower":
                inputMatches = inputRegexes[1].matcher(input).matches();
                break;
            case "isDigit":
                inputMatches = inputRegexes[2].matcher(input).matches();
                break;
            case "isPunct":
                inputMatches = inputRegexes[3].matcher(input).matches();
                break;
        }
        parameterMap.put(parameter, inputMatches);
        return inputMatches;
    }

    private static double progressCheck( boolean isDigit,boolean isPunct,
                                      boolean isLonger, boolean isLower, boolean isUpper){
        if(isLonger){
            if(isPunct && isLower && isUpper && isDigit){
                return 100;
            }
            else if (!(!isPunct && !isLower && !isUpper && !isDigit)){
                return 75;
            }
        }
        else{
            if(isPunct && isLower && isUpper && isDigit){
                return  50;
            }
            else if(!(!isPunct && !isDigit)||(isUpper&&isLower)){
                return 25;
            }
        }
        return 0;
    }
}
