package hr.fer.zavrsni.lozinka.lozinka.api.rest.controllers;

import hr.fer.zavrsni.lozinka.lozinka.api.model.User;
import hr.fer.zavrsni.lozinka.lozinka.api.rest.services.PasswordGeneratorService;
import hr.fer.zavrsni.lozinka.lozinka.api.rest.services.UsersService;
import hr.fer.zavrsni.lozinka.lozinka.exception.ForbiddenException;
import hr.fer.zavrsni.lozinka.lozinka.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/passwordGen")
public class PasswordGeneratorController {

    @Autowired
    private PasswordGeneratorService passwordGenerator;

    @Autowired
    private UsersService usersService;

    @GetMapping("")
    @ResponseBody
    public String generatePassword(String word,
                                    boolean isCapitalLetters, boolean isDigit,
                                   boolean isPunctuation) {
        return passwordGenerator.generatePassword(word, isCapitalLetters, isDigit, isPunctuation);
    }

    @GetMapping("Auto")
    @ResponseBody
    public String generatePasswordAuto(String word,
                                   String strength) {
        return passwordGenerator.generatePasswordAuto(word, strength);
    }

    @PutMapping("Quiz")
    public void putGeneratedPass(@RequestParam String generatedPass,@RequestParam String username){
        System.out.println("test2");
        User confirmedUser = usersService.getUserByUsername(username).orElseThrow(() -> new NotFoundException("User not found"));

        if(confirmedUser == null){
            System.out.println("test1");
            throw new ForbiddenException("No user found");
        }
        else{
            System.out.println("test");
            usersService.updateUserGeneratedPassword(confirmedUser, generatedPass);
        }
    }

    @GetMapping("Quiz")
    @ResponseBody
    public String generatePasswordAuto(String username) {
        User confirmedUser = usersService.getUserByUsername(username).orElseThrow(() -> new NotFoundException("User not found"));
        return confirmedUser.getGeneratedPass();
    }

}
