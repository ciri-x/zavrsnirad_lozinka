package hr.fer.zavrsni.lozinka.lozinka.api.rest.dtos;

import hr.fer.zavrsni.lozinka.lozinka.api.model.enums.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreateUserDTO {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private Role role;
    private boolean isConfirmed;
    private String confirmationToken;
}